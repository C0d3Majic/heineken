<!-- Dropdown Structure -->
<ul id='dropdown1' class='dropdown-content'>
  <li><a id="btnLogout">Cerrar Sesión</a></li>
</ul>
<div class="navbar-fixed">
  <nav>
    <div class="nav-wrapper"> 
    <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      <a href="{{url('inicio')}}" class="brand-logo center"><img class="logo_nav" style="width:85px;"src="/img/logo.svg"></a>
      <ul class="right hide-on-med-and-down">
        <li><a class="dropdown-trigger" href="#!" data-target="dropdown1" style="color:white; background-color:#343a40">{{$username}}<i class="material-icons right">arrow_drop_down</i></a></li>
      </ul>
    </div>
  </nav>
</div>