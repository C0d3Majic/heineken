<?php



namespace App\Http\Controllers;



use Illuminate\Routing\Controller as BaseController;

use App\Models\UserSessionsModel;

use App\Models\BarsModel;

use App\Models\LocationsModel;

use App\Models\ContestantsModel;

use App\Models\PrizesModel;

use App\Models\ZonesModel;

use App\Models\UsersModel;

use App\Models\UserTypesModel;

use Illuminate\Http\Request;

use Auth;

class SiteController extends BaseController
 
{

    public function show_view_dni()

    {

        //return response(array('status' => 'success', 'users' => $users));

        return view('dni');

    }



    public function show_view_codigo_premio(Request $request)

    {

        //return response(array('status' => 'success', 'users' => $users));
        if (!$request->session()->has('contestant'))   
            return redirect('/');
        return view('codigo_premio');

    }

    

    public function show_view_agradecimiento(Request $request)

    {

        if (!$request->session()->has('contestant'))   
            return redirect('/');
  
        $contestant =   $request->session()->get('contestant');
        $request->session()->forget('contestant');
        $prize      =   PrizesModel::where('owner', $contestant->id)->first();
        return view('agradecimiento')->with('prize',$prize->name);

    }



    public function show_view_login()

    {

        if(Auth::check())

            return redirect('inicio');

        else

            return view('login');

    }

    public function show_view_bases()
    {
        return view('bases');
    }



    public function show_view_dashboard()

    {

        if(Auth::check()){

            $locations      =   LocationsModel::all();

            $prizes         =   PrizesModel::whereNotNull('owner')->get();

            foreach($prizes as $prize)

            {

                $owner          =   ContestantsModel::find($prize->owner);

                $prize->owner   =   $owner->name . ' ' . $owner->last_name;

                $prize->mistery =   UsersModel::find($prize->mistery)->name;

            }

            $user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();

            return view('landing')->with('username',Auth::user()->name)->with('prizes',$prizes)->with('session_id',$user_session->id)->with('session_token',$user_session->session_token);

        }

        else{

                return redirect('login');

        }

    }



    public function show_view_prizes()

    {

        if(Auth::check() && Auth::user()->user_type == 1){

            $locations      =   LocationsModel::all();
            if(Auth::user()->location != 0)
                $prizes         =   PrizesModel::where('is_active',1)->where('location',Auth::user()->location)->get();
            else
                $prizes         =   PrizesModel::where('is_active',1)->get();

            foreach($prizes as $prize)

            {

                $prize->location    =   LocationsModel::find($prize->location)->name;

            }

            $user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();

            return view('prizes')->with('username',Auth::user()->name)->with('prizes',$prizes)->with('locations',$locations)->with('session_id',$user_session->id)->with('session_token',$user_session->session_token);

        }

        else{

            if(Auth::check())

                return redirect('participantes');

            else

                return redirect('login');

        }

    }



    public function redirect_landing(Request $request)
    {
        if (!$request->session()->has('contestant'))   
            return view('dni');
        else
            return view('codigo_premio');
    }



    public function show_view_contestants()

    {

        if(Auth::check()){
            if(Auth::user()->location != 0)
                $contestants    =   ContestantsModel::where('location',Auth::user()->location)->get();
            else
                $contestants    =   ContestantsModel::all();
            

            foreach($contestants as $contestant)

            {

                $contestant->bar        =   BarsModel::find($contestant->bar)->name;

                $contestant->fullname   =   $contestant->name . ' ' . $contestant->last_name;

                $contestant->location   =   LocationsModel::find($contestant->location)->name;

            }

            $bars           =   BarsModel::all();

            $locations      =   LocationsModel::all();

            $user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();

            return view ('contestants')->with('username',Auth::user()->name)->with('contestants_array',$contestants)->with('locations',$locations)->with('bars',$bars)->with('session_id',$user_session->id)->with('session_token',$user_session->session_token);

        }

        else
        {
            return redirect('login');
        }

        

    }



    public function show_view_recuperar()

    {

        return view ('recuperar_contraseña');

    }



    public function show_view_bars()

    {

        if(Auth::check()){

            if(Auth::user()->location != 0){
                if(Auth::user()->location == 1)
                    $bars  =   BarsModel::where('zone',1)->get();
                else
                    $bars  =   BarsModel::where('zone','!=',1)->get();
            }
            else
                $bars           =   BarsModel::all();
            

            $zones          =   ZonesModel::all();
            foreach($zones as $zone)
            {

                $zone->name     =   LocationsModel::find($zone->location)->name . ' ' . $zone->name;

            }

            foreach($bars as $bar)
            {

                $zone           =   ZonesModel::find($bar->zone);

                $location       =   LocationsModel::find($zone->location)->name;

                $bar->zone      =   $location . ' ' . $zone->name;

                $bar->visited_by_admin      =   ContestantsModel::where('bar',$bar->id)->first() ? 1 : 0;

                $bar->visited_by_mistery    =   0;
                $bar_contestants    =   ContestantsModel::where('bar',$bar->id)->get();
                
                foreach($bar_contestants as $contestant)
                {
                    $contestant_won_prize   =   PrizesModel::where('owner',$contestant->id)->first() ? 1 : 0;
                    if($contestant_won_prize)
                    {
                        $bar->visited_by_mistery    =   1;
                        break;
                    }    
                }
            }

            $user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();

            return view('bars')->with('username',Auth::user()->name)->with('zones',$zones)->with('bars',$bars)->with('session_id',$user_session->id)->with('session_token',$user_session->session_token);

        }

        else

            return redirect('login');

        

    }



    public function show_view_zones()

    {

        if(Auth::check() && Auth::user()->user_type == 1){

            $zones          =   ZonesModel::all();

            $locations      =   LocationsModel::all();

            foreach($zones as $zone)

            {

                $zone->location     =   LocationsModel::find($zone->location)->name;

            }

            $user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();

            return view('zones')->with('username',Auth::user()->name)->with('locations',$locations)->with('zones',$zones)->with('session_id',$user_session->id)->with('session_token',$user_session->session_token);

        }

        else{

            if(Auth::check())

                return redirect('participantes');

            else

                return redirect('login');

        }

            

    }



    public function show_view_users()

    {   

        if(Auth::check() && Auth::user()->user_type == 1){

            $users          =   UsersModel::all();
            $locations      =   LocationsModel::all();

            foreach($users as $user)

                $user->type =   UserTypesModel::find($user->user_type)->name;

            $user_types     =   UserTypesModel::all();

            $user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();

            return view('users')->with('locations',$locations)->with('username',Auth::user()->name)->with('user_types',$user_types)->with('users',$users)->with('session_id',$user_session->id)->with('session_token',$user_session->session_token);

        }

        else{

            if(Auth::check())

                return redirect('participantes');

            else

                return redirect('login');

        }

    }

    public function show_view_background()
    {
        return view('background');
    }

}

