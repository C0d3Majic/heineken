<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use App\Models\UserSessionsModel;
use App\Models\BarsModel;
use App\Models\LocationsModel;
use App\Models\ContestantsModel;
use App\Models\ZonesModel;
use App\Models\PrizesModel;
use Illuminate\Http\Request;
use Auth;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Contestant { 
    public $dni; 
} 

class PrizesController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['validate_dni','validate_code']]);
    }

    public function new(Request $request)
    {
        //Data Inputs
        $code           =   $request->input('code');
        $name           =   $request->input('name');
        $location       =   $request->input('location');
        $is_active      =   $request->input('active');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');

        if(empty($code))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Código del Premio'));
        if(empty($name))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Nombre del Premio'));
        if(empty($location))
            return  response(array('status' =>  'error',    'type'  =>  'Falto la ubicacion en la que esta disponible el Premio'));
        if(empty($is_active) && !is_numeric($is_active))
            return  response(array('status' =>  'error',    'type'  =>  'Falto determinar si el premio esta activo o no'));
    
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $code_already_exists    =   PrizesModel::where('code',$code)->first();
        if($code_already_exists)
            return  response(array('status' =>  'error',    'type'  =>  'Este Código ya esta asigando a un Premio'));
        
        $new_prize              =   new PrizesModel();
        $new_prize->name        =   ucwords($name);
        $new_prize->code        =   strtoupper($code);
        $new_prize->location    =   $location;
        $new_prize->is_active   =   $is_active;
        $new_prize->save();

        return response(array('status'  =>  'success'));
    }

    public function delete($id = null)
    {
        $prize     =   PrizesModel::find($id);
        $modal     =   '<h5>Eliminando el premio: <b>' . $prize->name . '</b></h5>';
        return response(array('status'  =>  'success',  'data'  =>  $modal));
    }

    public function delete_prize(Request $request)
    {
        //Data Inputs
        $prize_id  =   $request->input('prize_id');

        if(empty($prize_id))
            return response(array('status'  =>  'error',    'type'  =>  'Imposible eliminar este premio'));
        
        $prize_to_be_deleted            =   PrizesModel::find($prize_id);
        $prize_to_be_deleted->is_active =   0;
        $prize_to_be_deleted->save();
        
        return response(array('status'  =>  'success'));
    }

    public function edit($id = null)
    {
        $prize     =   PrizesModel::find($id);
        $locations      =   LocationsModel::all();
        $modal          =   '<h5>Editando el precio: <b>' . $prize->name . '</b></h5>';
        $modal          .=  '<div class="row">
                                <div class="input-field col m6 l6 s12">
                                    <i class="material-icons prefix">vpn_key</i>
                                    <input id="code" name="code" type="text" class="validate" value="'.$prize->code.'">
                                    <label for="code">Código</label>
                                </div>  
                                <div class="input-field col m6 l6 s12">
                                    <i class="material-icons prefix">card_giftcard</i>
                                    <input id="name" name="name" type="text" class="validate" value="'.$prize->name.'">
                                    <label for="name">Premio</label>
                                </div>      
                            </div>
                            <div class="row">
                                <div class="input-field col m6 l6 s12">
                                    <i class="material-icons prefix">power_settings_new</i>
                                    <select name="active" id="active">
                                        <option value="">Esta activo?</option>';
                                        if($prize->is_active == 1)
                                            $modal.='<option value="1" selected>Si</option>
                                                    <option value="0">No</option>';
                                        else
                                            $modal.='<option value="1">Si</option>
                                                    <option value="0" selected>No</option>';
                                    $modal.='</select>
                                    <label for="active">Activo</label>
                                </div>      
                                <div class="input-field col m6 l6 s12">
                                    <i class="material-icons prefix">landscape</i>
                                    <select name="location" id="location">
                                        <option value="">En que localidad esta activo el premio?</option>';
                                        foreach($locations as $location){
                                            if($location->id == $prize->location)
                                                $modal.='<option value="'.$location->id.'" selected>'.$location->name.'</option>';
                                            else
                                                $modal.='<option value="'.$location->id.'">'.$location->name.'</option>';
                                        }
                                    $modal.='   
                                    </select>
                                    <label for="location">Localidad</label>
                                </div>    
                            </div>  ';
        return response(array('status'  =>  'success',  'data'  =>  $modal));
    }

    /*public function generate_prizes()
    {
        $location = 2;
        $letters='abcdefghijklmnopqrstuvwxyz';
        for($x=0; $x<300; $x++)
        {
            $code_already_exists    =   true;
            while($code_already_exists)
            {                
                $code='';
                for($y=0; $y<6; ++$y){
                    $code.=rand(0,9);
                }
                $code.=$letters[rand(0,25)];
                $code_already_exists    =   PrizesModel::where('code',$code)->first();
            }    
            $location               =   $location == 1 ? 2 : 1;
            $new_prize              =   new PrizesModel();
            $new_prize->name        =   'Tarjeta de Regalo con valor de 10€ Decathon';
            $new_prize->code        =   strtoupper($code);
            $new_prize->location    =   $location;
            $new_prize->is_active   =   1;
            $new_prize->save();
        }
        for($x=0; $x<300; $x++)
        {
            $code_already_exists    =   true;
            while($code_already_exists)
            {                
                $code='';
                for($y=0; $y<6; ++$y){
                    $code.=rand(0,9);
                }
                $code.=$letters[rand(0,25)];
                $code_already_exists    =   PrizesModel::where('code',$code)->first();
            }    
            $location               =   $location == 1 ? 2 : 1;
            $new_prize              =   new PrizesModel();
            $new_prize->name        =   'Tarjeta de Regalo con valor de 10€ Media Mark';
            $new_prize->code        =   strtoupper($code);
            $new_prize->location    =   $location;
            $new_prize->is_active   =   1;
            $new_prize->save();
        }
        for($x=0; $x<300; $x++)
        {
            $code_already_exists    =   true;
            while($code_already_exists)
            {                
                $code='';
                for($y=0; $y<6; ++$y){
                    $code.=rand(0,9);
                }
                $code.=$letters[rand(0,25)];
                $code_already_exists    =   PrizesModel::where('code',$code)->first();
            }    
            $location               =   $location == 1 ? 2 : 1;
            $new_prize              =   new PrizesModel();
            $new_prize->name        =   'Tarjeta de Regalo con valor de 10€ El Corte Inglés';
            $new_prize->code        =   strtoupper($code);
            $new_prize->location    =   $location;
            $new_prize->is_active   =   1;
            $new_prize->save();
        }

        for($x=0; $x<100; $x++)
        {
            $code_already_exists    =   true;
            while($code_already_exists)
            {                
                $code='';
                for($y=0; $y<6; ++$y){
                    $code.=rand(0,9);
                }
                $code.=$letters[rand(0,25)];
                $code_already_exists    =   PrizesModel::where('code',$code)->first();
            }    
            $location               =   $location == 1 ? 2 : 1;
            $new_prize              =   new PrizesModel();
            $new_prize->name        =   'Entrada a Evento Heineken';
            $new_prize->code        =   strtoupper($code);
            $new_prize->location    =   $location;
            $new_prize->is_active   =   1;
            $new_prize->save();

        }
        for($x=0; $x<12; $x++)
        {
            $code_already_exists    =   true;
            while($code_already_exists)
            {                
                $code='';
                for($y=0; $y<6; ++$y){
                    $code.=rand(0,9);
                }
                $code.=$letters[rand(0,25)];
                $code_already_exists    =   PrizesModel::where('code',$code)->first();
            }    
            $location               =   $location == 1 ? 2 : 1;
            $new_prize              =   new PrizesModel();
            $new_prize->name        =   'Suscripcion de 6 meses Netflix';
            $new_prize->code        =   strtoupper($code);
            $new_prize->location    =   $location;
            $new_prize->is_active   =   1;
            $new_prize->save();

        //return response(array('status'  =>  'success'));
        }
    }*/

    public function export(Request $request)
    {
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
                    ->setCreator("Salvador Jimenez")
                    ->setLastModifiedBy("Salvador Jimenez")
                    ->setTitle("Premios Recomienda HK")
                    ->setSubject("Lista de premios")
                    ->setDescription(
                        "Documento de Excel con los premios de la plataforma recomiendahk.com"
                    )
                    ->setKeywords("RecomiendaHK")
                    ->setCategory("Premios Exportados");
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1',  'Código');
        $sheet->setCellValue('B1',  'Premio');
        $sheet->setCellValue('C1',  'Activo');
        $sheet->setCellValue('D1',  'Ubicación');
        $x=2;
        $prizes =   PrizesModel::all();
        foreach($prizes as $prize)
        {
            $sheet->setCellValue('A'.$x,  $prize->code);
            $sheet->setCellValue('B'.$x,  $prize->name);
            $sheet->setCellValue('C'.$x,  $prize->is_active ? 'Si' : 'No');
            $sheet->setCellValue('D'.$x,  LocationsModel::find($prize->location)->name);
            $x++;
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save('premios.xlsx');
        return response(array('status'  =>  'success'));
    }

    public function save(Request $request)
    {
        //Data Inputs
        $code           =   $request->input('code');
        $name           =   $request->input('name');
        $location       =   $request->input('location');
        $is_active      =   $request->input('active');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');
        $prize_id       =   $request->input('prize_id');
        
        if(empty($code))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Código del Premio'));
        if(empty($name))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Nombre del Premio'));
        if(empty($location))
            return  response(array('status' =>  'error',    'type'  =>  'Falto la ubicacion en la que esta disponible el Premio'));
        if(empty($is_active) && !is_numeric($is_active))
            return  response(array('status' =>  'error',    'type'  =>  'Falto determinar si el premio esta activo o no'));
    
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        $prize  =   PrizesModel::find($prize_id);
        if(strcasecmp($prize->code,$code)!=0)
        {
            $code_already_exists    =   PrizesModel::where('code',$code)->first();
            if($code_already_exists)
                return  response(array('status' =>  'error',    'type'  =>  'Este Código ya esta asigando a un Premio'));
        }

        $prize->name        =   ucwords($name);
        $prize->code        =   strtoupper($code);
        $prize->location    =   $location;
        $prize->is_active   =   $is_active;
        $prize->save();
        return response(array('status'  =>  'success'));
    }

    public function validate_dni(Request $request)
    {
        $dni            =   $request->input('dni');

        if(empty($dni))
            return response(array('status'  =>  'error',    'type'  =>  'Olvidaste poner un DNI!'));
           
        /*$user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));*/
        
        $contestant     =   ContestantsModel::where('dni',$dni)->first();
        if(!$contestant)
            return response(array('status'  =>  'error',    'type'  =>  'DNI no valido'));

        
        if ($request->session()->has('contestant'))
            $request->session()->flush();
        
        session(['contestant' => $contestant]);
        return response(array('status'  =>  'success'));
    }

    public function validate_code(Request $request)
    {
        $code           =   $request->input('code');
        if(empty($code))
            return response(array('status'  =>  'error',    'type'  =>  'Falto el Codigó'));
        /*$user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));*/
        
        if (!$request->session()->has('contestant'))   
            return response(array('status'  =>  'error',    'type'  =>  'No haz procesado previamente un DNI'));
        $contestant     =   $request->session()->get('contestant');
        if(!$contestant)
            return response(array('status'  =>  'error',    'type'  =>  'DNI no valido'));
        $contestant_won_prize    =   PrizesModel::where('code', $code)->whereNull('owner')->first();  
        if(!$contestant_won_prize)
            return response(array('status'  =>  'error',    'type'  =>  'Este Codigó No es Valido'));
            
        $contestant_won_prize->owner    =   $contestant->id;
        //$contestant_won_prize->mistery  =   Auth::user()->id; 
        $contestant_won_prize->date     =   date('Y-m-d');
        $contestant_won_prize->save();         
        return response(array('status'  =>  'success'));
    }

}