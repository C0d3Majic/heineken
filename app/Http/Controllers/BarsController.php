<?php



namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

use App\Models\UserSessionsModel;

use App\Models\BarsModel;

use App\Models\LocationsModel;

use App\Models\ContestantsModel;

use App\Models\ZonesModel;

use App\Models\PrizesModel;

use Illuminate\Http\Request;

use Auth;



use PhpOffice\PhpSpreadsheet\Spreadsheet;

use PhpOffice\PhpSpreadsheet\Writer\Xlsx;



class BarsController extends BaseController

{

    public function __construct()

    {

        $this->middleware('auth');

    }

    public function export(Request $request)
    {
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
                    ->setCreator("Salvador Jimenez")
                    ->setLastModifiedBy("Salvador Jimenez")
                    ->setTitle("Establecimientos Recomienda HK")
                    ->setSubject("Lista de premios")
                    ->setDescription(
                        "Documento de Excel con los establecimientos de la plataforma recomiendahk.com"
                    )
                    ->setKeywords("RecomiendaHK")
                    ->setCategory("Establecimientos Exportados");
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1',  'Nombre');
        $sheet->setCellValue('B1',  'Dirección');
        $sheet->setCellValue('C1',  'Propietario');
        $sheet->setCellValue('D1',  'Zona');
        $x=2;
        $bars =   BarsModel::all();
        foreach($bars as $bar)
        {
            $zone   =   ZonesModel::find($bar->zone);
            $sheet->setCellValue('A'.$x,  $bar->name);
            $sheet->setCellValue('B'.$x,  $bar->address);
            $sheet->setCellValue('C'.$x,  $bar->owner);
            $sheet->setCellValue('D'.$x,  LocationsModel::find($zone->location)->name . ' ' .$zone->name);
            $x++;
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save('establecimientos.xlsx');
        return response(array('status'  =>  'success'));
    }



    public function new(Request $request)

    {

        //Data Inputs

        $name           =   $request->input('name');

        $address        =   $request->input('address');

        $owner          =   $request->input('owner');

        $zone           =   $request->input('zone');

        $session_id     =   $request->input('session_id');

        $session_token  =   $request->input('session_token');

        $bases          =   $request->input('bases');

        if($bases != 1)

            return response(array('status'  =>  'error',    'type'  =>  'No haz aceptado los Terminos y Condiciones'));

        

        if(empty($name))

            return  response(array('status' =>  'error',    'type'  =>  'Falto el Nombre del Establecimiento'));

        if(empty($address))

            return  response(array('status' =>  'error',    'type'  =>  'Falto la Dirección del Establecimiento'));

        if(empty($owner))

            return  response(array('status' =>  'error',    'type'  =>  'Falto el Nombre del Propietario del Establecimiento'));

        if(empty($zone))

            return  response(array('status' =>  'error',    'type'  =>  'Falto la Zona en la que se ubica el Establecimiento'));

    

        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();

        if(!$user_session)

            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));

        

        $name_already_exists    =   BarsModel::where('name',$name)->where('zone',$zone)->first();

        if($name_already_exists)

            return  response(array('status' =>  'error',    'type'  =>  'Este Nombre ya esta asignado a un Establacimiento en esta Zona'));

        

        $new_bar              =   new BarsModel();

        $new_bar->name        =   ucwords(mb_strtolower ($name));

        $new_bar->address     =   ucwords(mb_strtolower ($address));

        $new_bar->zone        =   $zone;

        $new_bar->owner       =   ucwords(mb_strtolower ($owner));

        $new_bar->save();



        return response(array('status'  =>  'success'));

    }



    public function delete($id = null)

    {

        $bar        =   BarsModel::find($id);

        $modal      =   '<h5>Eliminando el Establecimiento: <b>' . $bar->name . '</b></h5>';

        return response(array('status'  =>  'success',  'data'  =>  $modal));

    }



    public function delete_bar(Request $request)

    {

        //Data Inputs

        $bar_id  =   $request->input('bar_id');



        if(empty($bar_id))

            return response(array('status'  =>  'error',    'type'  =>  'Imposible eliminar este Establecimiento'));

        $bar_has_contestants_linked_to_it   =   ContestantsModel::where('bar',$bar_id)->first();

        if($bar_has_contestants_linked_to_it)
            return response(array('status'  =>  'error',    'type'  =>  'Imposible eliminar este Establecimiento ya que tiene vinculados participantes'));

        $bar_to_be_deleted   =   BarsModel::find($bar_id);

        $bar_to_be_deleted->delete();

        

        return response(array('status'  =>  'success'));

    }

    public function new_contestant($id = null)
    {
        $bar            =   BarsModel::find($id);
        $modal          =   '<h5>Agregando Participante al Establecimiento: <b>' . $bar->name . '</b></h5>';
        $modal          .=  '<div class="row">

                                <div class="input-field col m6 l6 s12">
                                
                                    <i class="material-icons prefix">person</i>
                                    
                                    <input id="new_participant_name" name="new_participant_name" type="text" class="validate">
                                    
                                    <label for="new_participant_name">Nombre(s)</label>
                                
                                </div>
                            
                                <div class="input-field col m6 l6 s12">
                                
                                    <i class="material-icons prefix">person</i>
                                    
                                    <input id="new_participant_lastname" name="new_participant_lastname" type="text" class="validate">
                                    
                                    <label for="new_participant_lastname">Apellidos</label>
                                
                                </div>
                            
                            </div>
                            
                            <div class="row">      
                            
                                <div class="input-field col m6 l6 s12">
                                
                                    <i class="material-icons prefix">contact_mail</i>
                                    
                                    <input id="new_participant_dni" name="new_participant_dni" type="text" class="validate">
                                    
                                    <label for="new_participant_dni">DNI</label>
                                
                                </div>    
                            
                                <div class="input-field col m6 l6 s12">
                                
                                    <i class="material-icons prefix">mail</i><input autocomplete="off" id="new_participant_email" name="new_participant_email" type="email" class="validate">
                                    
                                    <label for="new_participant_email">Correo electrónico</label>
                                
                                </div>        
                            
                            </div>
                            
                                <div class="row center">
                            
                                <label>
                            
                                    <input id="new_participant_bases" name="new_participant_bases" type="checkbox" />
                            
                                    <span>He leído y acepto los <a class="modal-trigger" href="#modal4">términos y condiciones</a> del concurso.</span>
                            
                                </label>
                            
                                </div>';
        return response(array('status'  =>  'success',  'data'  =>  $modal));
    }

    public function new_participant(Request $request)
    {
        $bar_id         =   $request->input('bar_id');
        $name           =   $request->input('name');
        $last_name      =   $request->input('lastname');
        $dni            =   $request->input('dni');
        $email          =   $request->input('email');
        $bases          =   $request->input('bases');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');
        $acepto_participar  =   $request->input('acepto_participar');
        $acepto_email       =   $request->input('acepto_email');
        $acepto_sorteo      =   $request->input('acepto_sorteo');
        $dinamic_data       =   $request->input('dinamicos');

        if($dinamic_data>=1)
        {
            for($x=1;$x<=$dinamic_data; $x++)
            {
                if(empty($request->input('name_'.$x)))
                    return response(array('status'  =>  'error',    'type'  =>  'Falto el nombre del Participante Extra '.$x));
                if(empty($request->input('lastname_'.$x)))
                    return response(array('status'  =>  'error',    'type'  =>  'Falto el apellido del Participante Extra '.$x));
                if(empty($request->input('dni_'.$x)))
                    return response(array('status'  =>  'error',    'type'  =>  'Falto el dni del Participante Extra '.$x));
                if(empty($request->input('email_'.$x)))
                    return response(array('status'  =>  'error',    'type'  =>  'Falto el email del Participante Extra '.$x));
            }
        }

        if($bases != 1)
            return response(array('status'  =>  'error',    'type'  =>  'No haz aceptado los Terminos y Condiciones'));
        if($acepto_participar != 1)
            return response(array('status'  =>  'error',    'type'  =>  'No haz aceptado Participar en La Promocion'));
        /*if($acepto_email != 1)
            return response(array('status'  =>  'error',    'type'  =>  'No haz aceptado recibir Emails'));*/
        if($acepto_sorteo != 1)
            return response(array('status'  =>  'error',    'type'  =>  'No haz aceptado la Utilización de tus Datos en el sorteo'));
        
        if(empty($name))
            return response(array('status'  =>  'error',    'type'  =>  'Falto el nombre del Participante'));
        if(empty($last_name))
            return response(array('status'  =>  'error',    'type'  =>  'Falto el apellido del Participante'));
        if(empty($email))
            return response(array('status'  =>  'error',    'type'  =>  'Falto el email del Participante'));
        if(empty($dni))
            return response(array('status'  =>  'error',    'type'  =>  'Falto el dni del Participante'));
     
        /*$email_already_in_use   =   ContestantsModel::where('email',$email)->first();
        if($email_already_in_use)
            return response(array('status'  =>  'error',    'type'  =>  'Este email ya esta en uso con otro participante'));
        */
        
        $dni_already_in_use   =   ContestantsModel::where('dni',$dni)->first();
        if($dni_already_in_use)
            return response(array('status'  =>  'error',    'type'  =>  'Este DNI ya esta en uso con otro participante'));
        
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();

        if(!$user_session)

            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
            
        $new_participant                =   new ContestantsModel();
        $new_participant->name          =   $name;
        $new_participant->last_name     =   $last_name;
        $new_participant->email         =   $email;
        $new_participant->dni           =   $dni;
        $new_participant->bar           =   $bar_id;
        $bar                            =   BarsModel::find($bar_id);
        $location                       =   ZonesModel::find($bar->zone)->location;
        $new_participant->location      =   $location; 
        $new_participant->save();

        if($dinamic_data>=1)
        {
            for($x=1;$x<=$dinamic_data; $x++)
            {
                $new_participant                =   new ContestantsModel();
                $new_participant->name          =   $request->input('name_'.$x);
                $new_participant->last_name     =   $request->input('lastname_'.$x);
                $new_participant->email         =   $request->input('email_'.$x);
                $new_participant->dni           =   $request->input('dni_'.$x);
                $new_participant->bar           =   $bar_id;
                $bar                            =   BarsModel::find($bar_id);
                $location                       =   ZonesModel::find($bar->zone)->location;
                $new_participant->location      =   $location; 
                $new_participant->save();
            }
        }
        return response(array('status'  =>  'success'));
    }



    public function edit($id = null)

    {

        $bar            =   BarsModel::find($id);

        $locations      =   LocationsModel::all();

        $zones          =   ZonesModel::all();

        $modal          =   '<h5>Editando el Establecimiento: <b>' . $bar->name . '</b></h5>';

        $modal          .=  '<div class="row">

                                <div class="input-field col m6 l6 s12">

                                    <i class="material-icons prefix">local_bar</i>

                                    <input id="name" name="name" type="text" class="validate" value="'.$bar->name.'">

                                    <label for="name">Nombre del establecimiento</label>

                                </div>  

                                <div class="input-field col m6 l6 s12">

                                    <i class="material-icons prefix">home</i>

                                    <input id="address" name="address" type="text" class="validate" value="'.$bar->address.'">

                                    <label for="address">Dirección</label>

                                </div>      

                            </div>

                            <div class="row">

                                <div class="input-field col m6 l6 s12">

                                    <i class="material-icons prefix">people</i>

                                    <input id="owner" name="owner" type="text" class="validate" value="'.$bar->owner.'">

                                    <label for="owner">Propietario</label>

                                </div>      

                                <div class="input-field col m6 l6 s12">

                                    <i class="material-icons prefix">landscape</i>

                                    <select name="zone" id="zone">

                                        <option value="">En que zona se encuentra el establecimiento?</option>';

                                        foreach($zones as $zone)

                                        {

                                            $zone->name     =   LocationsModel::find($zone->location)->name . ' ' . $zone->name;

                                            if($zone->id == $bar->zone)

                                                $modal.='<option value="'.$zone->id.'" selected>'.$zone->name.'</option>';

                                            else

                                                $modal.='<option value="'.$zone->id.'">'.$zone->name.'</option>';

                                        }

                                    $modal.='</select>

                                    <label for="zone">Zona</label>

                                </div>    

                            </div>';

        return response(array('status'  =>  'success',  'data'  =>  $modal));

    }



    public function save(Request $request)

    {

        //Data Inputs

        $name           =   $request->input('name');

        $address        =   $request->input('address');

        $owner          =   $request->input('owner');

        $zone           =   $request->input('zone');

        $session_id     =   $request->input('session_id');

        $session_token  =   $request->input('session_token');

        $bar_id         =   $request->input('bar_id');



        if(empty($name))

            return  response(array('status' =>  'error',    'type'  =>  'Falto el Nombre del Establecimiento'));

        if(empty($address))

            return  response(array('status' =>  'error',    'type'  =>  'Falto la Dirección del Establecimiento'));

        if(empty($owner))

            return  response(array('status' =>  'error',    'type'  =>  'Falto el Nombre del Propietario del Establecimiento'));

        if(empty($zone))

            return  response(array('status' =>  'error',    'type'  =>  'Falto la Zona en la que se ubica el Establecimiento'));

    

        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();

        if(!$user_session)

            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));

        

        $bar    =   BarsModel::find($bar_id);

        if(strcasecmp($bar->name,$name)!=0)

        {

            $name_already_exists    =   BarsModel::where('name',$name)->where('zone',$zone)->first();

            if($name_already_exists)

                return  response(array('status' =>  'error',    'type'  =>  'Este Nombre ya esta asignado a un Establacimiento en esta Zona'));

        }

        

        $bar->name        =   ucwords(mb_strtolower ($name));

        $bar->address     =   ucwords(mb_strtolower ($address));

        $bar->zone        =   $zone;

        $bar->owner       =   ucwords(mb_strtolower ($owner));

        $bar->save();



        return response(array('status'  =>  'success'));

    }

}