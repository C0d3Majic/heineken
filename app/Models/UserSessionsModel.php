<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class UserSessionsModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_sessions';

}