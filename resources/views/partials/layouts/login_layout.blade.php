<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
      @include("partials.headers")
      @include("partials.styles.login_styles")
    </head>
    <body>
      @yield("content")

      @include("partials.general_footer")
      @include("partials.scripts.general_scripts")
      @include("partials.jquery.login_jquery")
    </body>
</html>