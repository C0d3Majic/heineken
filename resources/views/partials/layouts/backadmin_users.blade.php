<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
      @include("partials.headers")
      @include("partials.styles.backadmin_styles")
    </head>
    <body>
      @include("partials.backadmin_menu")
      @yield("content")
      @include("partials.general_footer")
      @include("partials.scripts.general_scripts")
      @include("partials.jquery.users_jquery")
    </body>
</html>