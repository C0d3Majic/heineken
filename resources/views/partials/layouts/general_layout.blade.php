<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
      @include("partials.headers")
      @include("partials.styles.general_styles")
    </head>
    <body>
      @yield("content")

      @include("partials.general_footer")
      @include("partials.scripts.general_scripts")
      @include("partials.jquery.general_jquery")
    </body>
</html>