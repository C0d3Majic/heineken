@extends("partials.layouts.backadmin_contestants")



@section("content")

<!--

  Content Section Start

-->

<main>

  <container>

    <ul id="slide-out" class="sidenav sidenav-fixed nav" style="top:64px; background-color:#343a40">

      @if(Auth::check() && Auth::user()->user_type == 1)

        <li><a href="{{url('inicio')}}" style="color:#fff"><i class="material-icons" style="color:#fff">home</i>Inicio</a></li>

        <li><a href="{{url('premios')}}" style="color:#fff"><i class="material-icons" style="color:#fff">card_giftcard</i>Premios</a></li>

        <li><a href="{{url('participantes')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Participantes</a></li>

        <li><a href="{{url('establecimientos')}}" style="color:#fff"><i class="material-icons" style="color:#fff">local_bar</i>Establecimientos</a></li>        

        <!--li><a href="{{url('zonas')}}" style="color:#fff"><i class="material-icons" style="color:#fff">landscape</i>Zonas</a></li-->      

        <li><a href="{{url('usuarios')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Usuarios</a></li>        

        <li><a id="btnLogout2"style="color:#fff"><i class="material-icons" style="color:#fff">power_settings_new</i>Cerrar Sesión</a></li>        

      @else
        <li><a href="{{url('inicio')}}" style="color:#fff"><i class="material-icons" style="color:#fff">home</i>Inicio</a></li>

        <li><a href="{{url('participantes')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Participantes</a></li>

        <li><a href="{{url('establecimientos')}}" style="color:#fff"><i class="material-icons" style="color:#fff">local_bar</i>Establecimientos</a></li>        

        <li><a id="btnLogout2"style="color:#fff"><i class="material-icons" style="color:#fff">power_settings_new</i>Cerrar Sesión</a></li>        

      @endif

    </ul>

    <div class="row">

      <div class="col xl9 offset-xl3 l8 offset-l4 m12 s12">

          <h3 class="title">Participantes</h3>        

      </div>  

      <div class="col xl9 offset-xl3 l8 offset-l4 m12 s12">

        <div class="card blue-white darken-1">

          <div class="card-content black-text">

            <div class="row">

              <div class="col s12" style="overflow-x:auto;"> 

                <table id="orders" class="striped">

                  <thead>

                      <tr>

                          <th>Nombre</th>

                          <th>Local</th>

                          <th>DNI</th>

                          <th>Correo electrónico</th>

                          <th>Isla</th>

                          <th>Acciones</th>

                      </tr>

                  </thead>

                  <tbody>

                      @foreach($contestants_array as $contestant)

                      <tr>

                      <td>{{$contestant->fullname}}</td>

                      <td>{{$contestant->bar}}</td>

                      <td>{{$contestant->dni}}</td>

                      <td>{{$contestant->email}}</td>

                      <td>{{$contestant->location}}</td>

                      <td>

                          <a id="editIcon" class="editContestant small material-icons tooltipped" data-contestant_id="{{$contestant->id}}" data-position="bottom" data-delay="50" data-tooltip="Editar"><i class="small material-icons">mode_edit</i></a>

                          <a class="deleteContestant small material-icons tooltipped" data-contestant_id="{{$contestant->id}}" data-position="bottom" data-delay="50" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a>

                      </td>  

                      </tr>

                      @endforeach

                  </tbody>

                </table>

              </div>

            </div>

          </div>

        </div>    

      </div>            

    </div>    

    

    <div class="row">

      <div class="col l2 push-l6 m2 push-m3 s6">

        <a id="btnAgregar" class="waves-effect waves-light btn modal-trigger" href="#modal3">Agregar nuevo participante</a>

      </div>

      <div class="col l4 push-l6 m4 push-m4 s6">

        <a id="btnExportar" class="waves-effect waves-light btn">Exportar lista de participantes</a>

      </div>

    </div>

  </container>

</main>

<!-- Modal Structure -->

<div id="modal1" class="modal">

  <div class="modal-content">

  </div>

  <div class="modal-footer">

    <div class="row">

      <div class="col s6">

        <a href="#!" id="btnGuardar" class="modal-action modal-close waves-effect waves-light btn">Guardar Cambios</a>

      </div>

      <div class="col s6">

        <a href="#!" id="btnCancelarEditar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar Cambios</a>

      </div>

    </div>

  </div>

</div>



<!-- Modal Structure -->

<div id="modal2" class="modal">

  <div class="modal-content">

    <h4>Delete Order</h4>

    <p>Do you really want to delete this order?</p>

  </div>

  <div class="modal-footer">

    <div class="row">

      <div class="col s6">

        <a href="#!" id="btnEliminar" class="modal-action modal-close waves-effect waves-light btn">Confirmar Eliminacion</a>

      </div>

      <div class="col s6">

        <a href="#!" id="btnCancelarEliminar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>

      </div>

    </div>

  </div>

</div>



<!-- Modal Structure -->

<div id="modal3" class="modal">

  <div class="modal-content">
    
    <span><h4 class="center" style="display: inline;">Llena los datos para el nuevo participante</h4> <i class="material-icons right modal-action modal-close">close</i></span>

    <div class="row">

      <div class="input-field col m6 l6 s12">

        <i class="material-icons prefix">person</i>

        <input id="new_name" name="new_name" type="text" class="validate">

        <label for="new_name">Nombre(s)</label>

      </div>

      <div class="input-field col m6 l6 s12">

        <i class="material-icons prefix">person</i>

        <input id="new_lastname" name="new_lastname" type="text" class="validate">

        <label for="new_lastname">Apellidos</label>

      </div>

    </div>

    <div class="row">      

      <div class="input-field col m6 l6 s12">

        <i class="material-icons prefix">contact_mail</i>

        <input id="new_dni" name="new_dni" type="text" class="validate">

        <label for="new_dni">DNI</label>

      </div>    

      <div class="input-field col m6 l6 s12">

        <i class="material-icons prefix">mail</i><input autocomplete="off" id="new_email" name="new_email" type="email" class="validate">

        <label for="new_email">Correo electrónico</label>

      </div>        

    </div>

    <div class="row">

      <div class="input-field col m6 l6 s12">

        <i class="material-icons prefix">local_bar</i>

        <select name="new_bar" id="new_bar">

          <option value="" disabled selected>Establecimiento</option>

          @foreach($bars as $bar)

            <option value="{{$bar->id}}">{{$bar->name}}</option>

          @endforeach

        </select>

        <label for="new_bar">Establecimiento</label>

      </div>

      <div class="input-field col m6 l6 s12">

        <i class="material-icons prefix">landscape</i>

        <select name="new_location" id="new_location">

          <option value="" disabled selected>Localidad</option>

          @foreach($locations as $location)

            <option value="{{$location->id}}">{{$location->name}}</option>

          @endforeach

        </select>

        <label for="new_location">Localidad</label>

      </div>

    </div>

    <div class="row center">

      <label>

        <input id="bases" name="bases" type="checkbox" />

        <span>He leído y acepto los <a class="modal-trigger" href="#modal4">términos y condiciones</a> del concurso.</span>

      </label>      

    </div>

    <div class="row center">

      <label>

        <input id="acepto_participar" name="acepto_participar" type="checkbox" />

        <span>Acepto participar en la promoción Recomendar HK periodo promocional 01 mayo al 15 agosto de 2018.</span>

      </label>      

    </div>

    <div class="row center">

      <label>

        <input id="acepto_email" name="acepto_email" type="checkbox" />

        <span>Acepto recibir email para informar de los ganadores durante el periodo promocional</span>

      </label>      

    </div>

    <div class="row center">

      <label>

        <input id="acepto_sorteo" name="acepto_sorteo" type="checkbox" />

        <span>Acepto la utilización de mis datos para participar en sorteos durante el periodo promocional</span>

      </label>      

    </div>



    <!--div class="row">

      <div class="input-field col m6 push-m3 l6 push-l3 s12">

        <i class="material-icons prefix">assignment_ind</i>

        <select id="new_user_type" name="new_activo">

          <option value="" disabled selected>Esta activo?</option>

          <option value="1">Mystery</option>

          <option value="2">Camarero</option>

        </select>

        <label for="new_activo">Tipo de Usuario</label>

      </div>      

    </div-->    



  </div>  

  <div class="modal-footer">

    <div class="row">

      <div class="col s6">

        <a href="#!" id="btnGuardarNuevo" class="modal-action modal-close waves-effect waves-light btn">Guardar</a>

      </div>

      <div class="col s6">

        <a href="#!" id="btnCancelar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>

      </div>

    </div>

  </div>

</div>



<!-- Modal Structure -->

<div id="modal4" class="modal">

  <div class="modal-content">

    <span><h4 class="center" style="display: inline;">Terminos y Condiciones</h4> <i class="material-icons right modal-action modal-close">close</i></span>
    <p><b>1. Objeto</b></br> 
Insular Canarias de Bebida, S.A. (en adelante, el “Organizador”), con CIF número A-38069704 y con domicilio social en el Polígono Industrial de Salinetas, calle Pescador 17 (35219) de Las Palmas, organiza y gestiona la promoción denominada “RECOMENDAR HK TIENE PREMIO” (en adelante, la “Acción promocional”), que se gestionará a través de la página web www.recomendarhk.com  (en adelante, la “Web”) cuya gestión y explotación está encomendada a T-time Below the line S.L.  (en adelante, la “Gestora”) y que será́ la encargada de la gestión de la presente Acción promocional.
</br> <b>2. Participación</b></br>  
Para poder participar en la presente Acción promocional se requiere lo siguiente: 
•	Ser mayor de edad. 
•	Ser empleado de un Punto de venta que posea Heineken de Botella.
•	Ser residente en el territorio español a efectos legales en el momento de haber participado en la presente Iniciativa (DNI o Tarjeta de residencia). 
•	Estar registrado en la Web de la Acción promocional. 
Cada Participante garantiza al Organizador la veracidad de los datos suministrados respondiendo de cualquier reclamación que reciba en caso de incumplimiento de tal garantía. 
Tendrán la condición de Participantes los usuarios que cumplan los requisitos anteriores y participen en la Acción promocional de acuerdo con los términos especificados en el apartado siguiente para el desarrolla de la misma (en adelante, los “Participantes”). 
Quedan excluidos de esta Iniciativa todo el personal laboral del Organizador y la empresa Gestora y familiares directos (hermanos, cónyuges, hijos y padres). 
</br> <b>3. Agente Secreto</b></br>  
La Gestora se encargará de visitar a los Participantes, para lo que organizará hasta 3 rutas por cada punto de venta inscrito en la Acción promocional mediante personal de su empresa en formato de cliente misterioso (en adelante Agente secreto) con la formación sobre las presentes condiciones de la presente Acción promocional y que actuará como un cliente y  no se identificará como Agente secreto hasta haber solicitado una cerveza y esperar la respuesta del Participante.  
El Agente secreto podrá solicitar a los Participantes:
•	Una cerveza, sin especificar marca.
•	Una caña, sin mas especificación.
•	Una Heineken.
•	Cualquier otra marca de Cerveza.
</br> <b>4. Duración.</b></br>  
El plazo de participación comienza el 15 de mayo de 2018 y finalizará el 15 de agosto de 2018.
</br> <b>5. Mecánica cliente misterioso.</b></br>  
Durante el plazo de participación cada punto de venta recibirá la visita de un Agente de reclutamiento para registrar al Punto de venta y los Participantes de cada punto de venta.
En la siguientes semanas podrán recibir hasta 3 visitas, de un Agente secreto. Para resultar premiado el Participante, deberá ofrecer al cliente misterioso una Heineken de Botella, para ello:
•	Deberá ofrecer una Heineken de Botella específicamente.
•	No será válido ofrecer tan sólo una Heineken sin mencionar “Botella”.
•	No será válido ofrecerla como última opción.
Cuando un Participante ofrezca correctamente la botella de Heineken al Agente secreto recibirá un cupón que deberá canjear en la Web, será la Web quien indicará el regalo que ha ganado de forma aleatoria, este regalo será entregado en el momento por el cliente misterioso.
Puede pasar que el Agente secreto realice una visita a un empleado de un punto de venta no registrado y por lo tanto no se podrá considerar Participante, en este caso se comunicará y podrá registrarse en el momento y pasar a ser Participante para las siguientes visitas dentro del periodo promocional, en este caso podría ampliarse en número de visitas de 3 a 4 visitas al punto de venta si el organizador así lo decidiera.
En el caso de no resultar premiado se le recordará la mecánica al Participante y se le explicará nuevamente como podrá resultar ganador en futuras visitas.
</br> <b>6. Mecánica sorteo Puntos de venta.</b></br>  
Al final del periodo promocional, se realizará un sorteo entre todos los Puntos de venta inscritos en la Acción promocional, el regalo a sortear:
2 Viajes Interinsulares, viaje, traslado, estancia en Hotel superior y Cena en Restaurante concertado, por un valor de 500€. 
1 Sorteo por provincia
Este sorteos se realizará mediante la aplicación Sorteados, según el orden de inscripción en la Web de la Acción promocional.
</br> <b>7. Premios directos Participantes </b></br>  
El Organizador ha dispuesto los siguientes regalos para la Acción promocional:
•	300 tarjetas de regalo MediaMarkt por valor de 10€.
•	300 tarjetas de regalo Decathlon por valor de 10€.
•	300 tarjetas de regalo El Corte Inglés por valor de 10€
•	100 entradas dobles a conciertos.
•	12 suscripciones de Netflix de 6 meses.
La entrega de los regalos se realizará aleatoriamente comprobando el cupón que hará entrega el cliente misterioso a los Participantes ganadores.

</br> <b>8. Premios mediante Sorteo para Participantes. </b></br>  
El Organizador ha dispuesto los siguientes regalos a sortear entre los Participantes ganadores de cada ronda, los sorteos se realizarán al finalizar cada ronda de Agente Secreto en las siguientes fechas:
1er Sorteo Semana 28 de mayo. (2 iWatch 42mm., 1 por provincia)
2º Sorteo Semana 18 Junio (2 iPad Mini, 1 por provincia)
3er Sorteo Semana 2 Julio (1 iPhone X 64GB, 1 para todos los participantes)
La entrega de los regalos se realizará personalmente la misma semana de la realización del sorteo dentro del periodo promocional.
</br> <b>9. Supervisión </b></br>  
Cualquier Participante que manipule los procedimientos de participación y/o que incumpla las bases contenidas en el presente documento será́ descalificado. También queda reservado el derecho de verificar por cualquier procedimiento que el Organizador estime apropiado que los ganadores cumplen con todos los requisitos de este documento. 
</br> <b>10. Modificaciones de las Bases y/o anexos </b></br>  
El Organizador se reserva el derecho a realizar modificaciones – incluyendo la cancelación o suspensión anticipada de la Iniciativa – y/o a añadir anexos sucesivos sobre estas bases por motivos técnicos, operativos, comerciales, o de participación, publicando siempre estas modificaciones y/o anexos en la dirección www.recomendarhk.com.
</br> <b>11. Protección de Datos de carácter personal </b></br>  
Insular Canarias de Bebida, S.A., titular del portal www.recomendarhk.com , informa a los usuarios del mismo, en adelante el “portal”, que Insular Canarias de Bebida, S.A., es responsable de los tratamientos que se realicen mediante este portal, salvo que otra cosa se informe en el tratamiento de que se trate.
Insular Canarias de Bebida, S.A. respeta la legislación vigente en materia de protección de datos personales, la privacidad de los usuarios y el secreto y seguridad de los datos personales, de conformidad con lo establecido en la legislación aplicable en materia de protección de datos, en concreto, el Reglamento 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016, adoptando para ello las medidas técnicas y organizativas necesarias para evitar la pérdida, mal uso, alteración, acceso no autorizado y robo de los datos personales facilitados, habida cuenta del estado de la tecnología, la naturaleza de los datos y los riesgos a los que están expuestos.
En concreto, se informa a los usuarios del portal que sus datos de carácter personal sólo podrán obtenerse para su tratamiento cuando sean adecuados, pertinentes y no excesivos en relación con el ámbito y las finalidades determinadas, explícitas y legítimas para las que se hayan obtenido.
Cuando se recaben los datos personales a través del portal, se informará previamente al usuario, de forma clara e inequívoca, de los siguientes extremos:
•	Existencia de un tratamiento de datos de carácter personal.
•	La identidad y los datos de contacto del responsable del tratamiento de datos personales obtenidos:
-	T-time Below the Line S.L. cif b76150903
-	hello@t-time.es
•	Los fines del tratamiento a que se destinan los datos personales y la base jurídica del tratamiento;
-	Confirmación de los regalos obtenidos.
-	Comunicación de los Puntos de Venta ganadores.
•	Los destinatarios o las categorías de destinatarios de los datos personales, en su caso.
•	El plazo o los criterios de conservación de la información;
- Durante el periodo promocional.
•	La existencia del derecho a solicitar al responsable del tratamiento el acceso a los datos personales relativos al interesado, y su rectificación o supresión, o la limitación de su tratamiento, el derecho a la portabilidad de los datos, o a oponerse al tratamiento. Asimismo, la manera de ejercitar los mencionados derechos.
•	Cuando el tratamiento esté basado en el consentimiento del interesado, la existencia del derecho a retirar el consentimiento en cualquier momento, sin que ello afecte a la licitud del tratamiento basado en el consentimiento previo a su retirada; Si el tratamiento no está basado en el consentimiento, su derecho a ejercer la oposición al tratamiento.
•	El derecho a presentar una reclamación ante una autoridad de control;
•	La existencia, si las hubiera, de decisiones automatizas, incluida la elaboración de perfiles y el ejercicio de derechos asociados a dicho tratamiento.

El usuario será el único responsable a causa de la cumplimentación de los formularios con datos falsos, inexactos, incompletos o no actualizados.
</br> <b>12. Conflictos y Ley Aplicable. </b></br>  
En caso de divergencia entre los usuarios y la interpretación de las presentes bases por parte del Organizador, serán competentes para conocer de los litigios que puedan plantearse los Juzgados y Tribunales de la ciudad de Las Palmas, renunciando expresamente los Participantes al fuero que pudiera corresponderles. 
La participación en esta Iniciativa supone la aceptación expresa de las presentes bases y la sumisión expresa de las decisiones interpretativas que de las mismas efectué el Organizador. 


<span style="float:right;font-weight:bold">En Las Palmas de Gran Canaria a 04 de mayo de 2018.</span>

</p>

  </div>

  <div class="modal-footer">

    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Aceptar</a>

  </div>

</div>

<!--

  Content Section End

-->

@endsection