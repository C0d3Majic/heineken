<script>

    $(document).ready(function(){

        $('.modal').modal({dismissible:false});        

        $('#orders').DataTable();

        $('.sidenav').sidenav();

        $('.tooltipped').tooltip();

        $(".dropdown-trigger").dropdown({

            hover:true,

            coverTrigger:false

        });

        $('select').formSelect({hover:false});

        var bar_id;        

         $('body').on('click', '.deleteBar', function(){

            bar_id = $(this).data('bar_id');

            $.ajax({

                // The URL for the request. variable set above

                url: "{{url('delete/bar')}}/"+bar_id,

                // Whether this is a POST or GET request

                type: "GET",

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

                // The type of data we expect back. can be json, html, text, etc...

                dataType : "json",

                // Code to run if the request succeeds;

                // the response is passed to the function

                success: function (response) {  

                if(response.status == 'error'){

                    var $toastContent = $('<span>'+ response.type +'</span>');

                    M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});

                }else if(response.status == 'success'){

                    $('.modal-content').html(response.data);

                    $('select').formSelect();

                    M.updateTextFields();

                    $('#modal2').modal('open');

                }

                },

                error: function(){

                //window.location.reload();

                var $toastContent = $('<span>Hubo un error en el servidor</span>');

                M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});

                }

            });

        });
        
        
        $('body').on('click', '.participanteBar', function(){

            bar_id = $(this).data('bar_id');
        });


        $('body').on('click', '.editBar', function(){

            bar_id = $(this).data('bar_id');

            $.ajax({

                // The URL for the request. variable set above

                url: "{{url('edit/bar')}}/"+bar_id,

                // Whether this is a POST or GET request

                type: "GET",

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

                // The type of data we expect back. can be json, html, text, etc...

                dataType : "json",

                // Code to run if the request succeeds;

                // the response is passed to the function

                success: function (response) {   

                    if(response.status == 'error'){

                        var $toastContent = $('<span>'+ response.type +'</span>');

                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});

                    }else if(response.status == 'success'){

                        $('.modal-content').html(response.data);

                        $('select').formSelect();

                        M.updateTextFields();

                        $('#modal1').modal('open');

                    }

                },

                error: function(){

                //window.location.reload();

                    var $toastContent = $('<span>Hubo un error en el servidor</span>');

                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});

                }

            });

        });



        $('#btnCancelarEditar').click(function(){

            M.toast({html: 'Edicion Cancelada!', classes: 'rounded', displayLength:1000, completeCallback: function(){window.location.reload()}});                    

        });



        $('#btnCancelarEliminar').click(function(){

            M.toast({html: 'Eliminacion Cancelada!', classes: 'rounded', displayLength:1000, completeCallback: function(){window.location.reload()}});                    

        });

        $('#btnCancelarNuevoParticipante').click(function(){

            //M.toast({html: 'Agregar Participante Cancelada!', classes: 'rounded', displayLength:1000, completeCallback: function(){/*window.location.reload()*/}});                    

        });


        $("#btnExportar").click(function(){            
            var jsonObject = {
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}"
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('export/bars')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:3000, completeCallback: function(){window.location.reload()}});
                    }else{
                        M.toast({html: 'Exportacion Exitosa!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.open("{{url('/')}}/establecimientos.xlsx", '_new')}}); 
                        
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        var is_checked = 0;
        var is_checked_participante =   0;
        $('#bases').change(function(){

            is_checked = this.checked ? 1 : 0;

        });

        $('#new_participant_bases').change(function(){

            is_checked_participante = this.checked ? 1 : 0;

        });

        var acepto_participar_checked = 0;

        $('#acepto_participar').change(function(){

            acepto_participar_checked = this.checked ? 1 : 0;

        });

        var acepto_email_checked = 0;

        $('#acepto_email').change(function(){

            acepto_email_checked = this.checked ? 1 : 0;

        });

        var acepto_sorteo_checked = 0;

        $('#acepto_sorteo').change(function(){

            acepto_sorteo_checked = this.checked ? 1 : 0;

        });
       
        $('.collapsible').collapsible();
        var agregadosDinamicamente = 0;
        $("#btnAgregarOtroParticipante").click(function(){
            agregadosDinamicamente++;
            $( "#contenidoDinamico" ).append('<li id="extra_'+agregadosDinamicamente+'">'+
                                                '<div class="collapsible-header"><i class="material-icons">people</i>Participante Extra '+agregadosDinamicamente+'</div>'+
                                                '<div class="collapsible-body">'+
                                                    '<span>'+
                                                        '<div class="row"> '+
                                                            '<div class="input-field col m6 l6 s12">'+
                                                                '<i class="material-icons prefix">person</i>'+
                                                                '<input id="new_participant_name_'+agregadosDinamicamente+'" name="new_participant_name_'+agregadosDinamicamente+'" type="text" class="validate">'+
                                                                '<label for="new_participant_name_'+agregadosDinamicamente+'">Nombre(s)</label>'+
                                                            '</div>'+
                                                            '<div class="input-field col m6 l6 s12">'+
                                                                '<i class="material-icons prefix">person</i>'+
                                                                '<input id="new_participant_lastname_'+agregadosDinamicamente+'" name="new_participant_lastname_'+agregadosDinamicamente+'" type="text" class="validate">'+
                                                                '<label for="new_participant_lastname_'+agregadosDinamicamente+'">Apellidos</label>'+
                                                            '</div>'+
                                                        '</div>'+
                                                        '<div class="row">' +     
                                                            '<div class="input-field col m6 l6 s12">'+
                                                                '<i class="material-icons prefix">contact_mail</i>'+
                                                                '<input id="new_participant_dni_'+agregadosDinamicamente+'" name="new_participant_dni_'+agregadosDinamicamente+'" type="text" class="validate">'+
                                                                '<label for="new_participant_dni_'+agregadosDinamicamente+'">DNI</label>'+                                          
                                                            '</div>'+
                                                            '<div class="input-field col m6 l6 s12">'+
                                                                '<i class="material-icons prefix">mail</i><input autocomplete="off" id="new_participant_email_'+agregadosDinamicamente+'" name="new_participant_email_'+agregadosDinamicamente+'" type="email" class="validate">'+
                                                                '<label for="new_participant_email_'+agregadosDinamicamente+'">Correo electrónico</label>'+
                                                            '</div>'+    
                                                        '</div>'+
                                                    '</span>'+
                                                '</div>'+ 
                                            '</li>');
        });

        $("#btnRemoverParticipanteExtra").click(function(){
            var element_name    =   '#extra_' +agregadosDinamicamente;
            if(agregadosDinamicamente>=1)
            {   
                $(element_name).remove();
                agregadosDinamicamente--;
            }else
                M.toast({html: 'Ya no hay mas participantes extra', classes: 'rounded red', displayLength:3000});
        });



        $('#btnGuardarNuevoParticipante').click(function(){

            var jsonObject = {

                session_id      :   "{{$session_id}}",

                session_token   :   "{{$session_token}}",

                name            :   $("#new_participant_name").val(),

                lastname        :   $("#new_participant_lastname").val(),

                email           :   $("#new_participant_email").val(),

                bases           :   is_checked_participante,

                dni             :   $("#new_participant_dni").val(),

                bar_id          :   bar_id,

                acepto_participar:  acepto_participar_checked,

                acepto_email    :   acepto_email_checked,

                acepto_sorteo   :   acepto_sorteo_checked,

                dinamicos       :   agregadosDinamicamente                

            }

            if(agregadosDinamicamente>=1)
            {
                for(var i=1; i<=agregadosDinamicamente; i++){
                    var newUser = "name_" + i;
                    var newValue = "#new_participant_name_" + i;
                    jsonObject[newUser] = $(newValue).val() ;
                    newUser= "email_" + i;
                    newValue = "#new_participant_email_" + i;
                    jsonObject[newUser] = $(newValue).val() ;
                    newUser= "lastname_" + i;
                    newValue = "#new_participant_lastname_" + i;
                    jsonObject[newUser] = $(newValue).val() ;
                    newUser= "dni_" + i;
                    newValue = "#new_participant_dni_" + i;
                    jsonObject[newUser] = $(newValue).val() ;
                }
            }
            console.log(jsonObject)
            $.ajax({

                // The URL for the request. variable set above

                url: "{{url('save/new/participant/bar')}}",

                // The data to send (will be converted to a query string). variable set above

                data: jsonObject,

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

                // Whether this is a POST or GET request

                type: "POST",

                // The type of data we expect back. can be json, html, text, etc...

                dataType : "json",

                // Code to run if the request succeeds;

                // the response is passed to the function

                success: function( msg ) {

                    if(msg.status == 'error'){

                        var $toastContent = $('<span>'+ msg.type +'</span>');

                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:3000, completeCallback: function(){/*window.location.reload()*/}});

                    }else{

                        M.toast({html: 'Participante Agregado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 

                    }

                },

                error: function(){

                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});

                }

            });

        });
        $('#btnEliminar').click(function(){

            var jsonObject = {

                session_id: "{{$session_id}}",

                session_token : "{{$session_token}}",

                bar_id : bar_id

            }

            $.ajax({

                // The URL for the request. variable set above

                url: "{{url('delete/bar')}}",

                // The data to send (will be converted to a query string). variable set above

                data: jsonObject,

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

                // Whether this is a POST or GET request

                type: "POST",

                // The type of data we expect back. can be json, html, text, etc...

                dataType : "json",

                // Code to run if the request succeeds;

                // the response is passed to the function

                success: function( msg ) {

                    if(msg.status == 'error'){

                        var $toastContent = $('<span>'+ msg.type +'</span>');

                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:3000, completeCallback: function(){window.location.reload()}});

                    }else{

                        M.toast({html: 'Establecimiento Eliminado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 

                    }

                },

                error: function(){

                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});

                }

            });

        })

        $('#btnLogout2').click(function(){

            // Start $.ajax() method

            var jsonObject = {

                session_id: "{{$session_id}}",

                session_token : "{{$session_token}}"

            }

            $.ajax({

            // The URL for the request. variable set above

            url: "{{url('user/logout')}}",

            // The data to send (will be converted to a query string). variable set above

            data: jsonObject,

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            },

            // Whether this is a POST or GET request

            type: "POST",

            // The type of data we expect back. can be json, html, text, etc...

            dataType : "json",

            // Code to run if the request succeeds;

            // the response is passed to the function

            success: function( msg ) {

                if(msg.status == 'error'){

                var $toastContent = $('<span>'+ msg.type +'</span>');

                M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});

                }else{

                window.location.reload();

                }

            },

            error: function(){

                M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});

            }

            });

        }); 

        $('#btnLogout').click(function(){

            // Start $.ajax() method

            var jsonObject = {

                session_id: "{{$session_id}}",

                session_token : "{{$session_token}}"

            }

            $.ajax({

                // The URL for the request. variable set above

                url: "{{url('user/logout')}}",

                // The data to send (will be converted to a query string). variable set above

                data: jsonObject,

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

                // Whether this is a POST or GET request

                type: "POST",

                // The type of data we expect back. can be json, html, text, etc...

                dataType : "json",

                // Code to run if the request succeeds;

                // the response is passed to the function

                success: function( msg ) {

                    if(msg.status == 'error'){

                    var $toastContent = $('<span>'+ msg.type +'</span>');

                    M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});

                    }else{

                        window.location.reload();

                    }

                },

                error: function(){

                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});

                }

            });

        });  



        $("#btnGuardarNuevo").click(function(){

             // Start $.ajax() method

             var jsonObject = {

                session_id      :   "{{$session_id}}",

                session_token   :   "{{$session_token}}",

                name            :   $("#new_name").val(),

                address         :   $("#new_address").val(),

                owner           :   $("#new_owner").val(),

                zone            :   $("#new_zone").val(),

                bases           :   is_checked

            }

            $.ajax({

                // The URL for the request. variable set above

                url: "{{url('new/bar')}}",

                // The data to send (will be converted to a query string). variable set above

                data: jsonObject,

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

                // Whether this is a POST or GET request

                type: "POST",

                // The type of data we expect back. can be json, html, text, etc...

                dataType : "json",

                // Code to run if the request succeeds;

                // the response is passed to the function

                success: function( msg ) {

                    if(msg.status == 'error'){

                        var $toastContent = $('<span>'+ msg.type +'</span>');

                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});

                    }else{

                        M.toast({html: 'Establecimiento Agregado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 

                    }

                },

                error: function(){

                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});

                }

            });

        });



        $("#btnGuardar").click(function(){

            // Start $.ajax() method

            var jsonObject = {

                session_id      :   "{{$session_id}}",

                session_token   :   "{{$session_token}}",

                name            :   $("#name").val(),

                address         :   $("#address").val(),

                owner           :   $("#owner").val(),

                zone            :   $("#zone").val(),

                bar_id          :   bar_id

            }

            $.ajax({

                // The URL for the request. variable set above

                url: "{{url('save/bar')}}",

                // The data to send (will be converted to a query string). variable set above

                data: jsonObject,

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

                // Whether this is a POST or GET request

                type: "POST",

                // The type of data we expect back. can be json, html, text, etc...

                dataType : "json",

                // Code to run if the request succeeds;

                // the response is passed to the function

                success: function( msg ) {

                    if(msg.status == 'error'){

                        var $toastContent = $('<span>'+ msg.type +'</span>');

                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:3000, completeCallback: function(){window.location.reload()}});

                    }else{

                        M.toast({html: 'Establecimiento Editado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 

                    }

                },

                error: function(){

                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});

                }

            });

        })

    });

</script>