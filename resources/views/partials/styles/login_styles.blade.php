<style>
    body {
        display: flex;
        min-height: 100vh;
        flex-direction: column;
    }

    main {
        flex: 1 0 auto;
    }

    body {
      background: #1DAD33 ;
    }

    footer{
        background-color: #1DAD33  !important;
    }

    @media (max-height: 640px) {
        footer{
            height:90px;
            padding-top:10px !important;
        }
    }
    /* label focus color */
    .input-field input[type=text]:focus + label {
        color: #000;
    }  
</style>