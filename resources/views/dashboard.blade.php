@extends("partials.layouts.backadmin_layout")

@section("content")
<!--
  Content Section Start-->

<main>
  <container>
    <ul id="slide-out" class="sidenav sidenav-fixed nav" style="top:64px; background-color:#343a40">
    @if(Auth::check() && Auth::user()->user_type == 1)
        <li><a href="{{url('inicio')}}" style="color:#fff"><i class="material-icons" style="color:#fff">home</i>Inicio</a></li>
        <li><a href="{{url('premios')}}" style="color:#fff"><i class="material-icons" style="color:#fff">card_giftcard</i>Premios</a></li>
        <li><a href="{{url('participantes')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Participantes</a></li>
        <li><a href="{{url('establecimientos')}}" style="color:#fff"><i class="material-icons" style="color:#fff">local_bar</i>Establecimientos</a></li>        
        <!--li><a href="{{url('zonas')}}" style="color:#fff"><i class="material-icons" style="color:#fff">landscape</i>Zonas</a></li-->      
        <li><a href="{{url('usuarios')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Usuarios</a></li>        
        <li><a id="btnLogout2"style="color:#fff"><i class="material-icons" style="color:#fff">power_settings_new</i>Cerrar Sesión</a></li>        
      @else
        <li><a href="{{url('participantes')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Participantes</a></li>
        <li><a href="{{url('establecimientos')}}" style="color:#fff"><i class="material-icons" style="color:#fff">local_bar</i>Establecimientos</a></li>        
        <li><a id="btnLogout2"style="color:#fff"><i class="material-icons" style="color:#fff">power_settings_new</i>Cerrar Sesión</a></li>        
      @endif
    </ul>
    
    <div class="row">
      <div class="col l9 offset-l3 m12 s12">
          <h3 class="title">Premios utilizados recientemente</h3>        
      </div>  
      <div class="col l9 offset-l3 m12 s12">
        <div class="card blue-white darken-1">
          <div class="card-content black-text">
            <div class="row">
              <div class="col s12">
                <table id="orders" class=" responsive-table">
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Premio</th>
                        <th>Ganador</th>
                        <th>Mistery</th>
                        <th>Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($prizes as $prize)
                      <tr>
                        <td>{{$prize->code}}</td>
                        <td>{{$prize->name}}</td>
                        <td>{{$prize->owner}}</td>
                        <td>{{$prize->mistery}}</td>
                        <td>{{$prize->date}}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>    
        </div>     
      </div>   
    </div>
  </container>
</main>

<!--
  Content Section End
-->
@endsection