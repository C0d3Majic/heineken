<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use App\Models\UserSessionsModel;
use App\Models\BarsModel;
use App\Models\LocationsModel;
use App\Models\ContestantsModel;
use App\Models\ZonesModel;
use App\Models\PrizesModel;
use Illuminate\Http\Request;
use Auth;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ZonesController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function new(Request $request)
    {
        //Data Inputs
        $name           =   $request->input('name');
        $location       =   $request->input('location');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');

        if(empty($name))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Nombre de la Zona'));
        if(empty($location))
            return  response(array('status' =>  'error',    'type'  =>  'Falto la Ubicacion de la Zona'));
      
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $name_already_exists    =   ZonesModel::where('name',$name)->where('location',$location)->first();
        if($name_already_exists)
            return  response(array('status' =>  'error',    'type'  =>  'Este Nombre ya esta asignado a una Zona en esa Ubicación'));
        
        $new_zone              =   new ZonesModel();
        $new_zone->name        =   ucwords(mb_strtolower ($name));
        $new_zone->location    =   $location;
        $new_zone->save();

        return response(array('status'  =>  'success'));
    }

    public function delete($id = null)
    {
        $zone               =   ZonesModel::find($id);
        $zone->ubicacion    =   LocationsModel::find($zone->location)->name;
        $modal      =   '<h5>Eliminando la Zona: <b>' . $zone->name . ' en ' . $zone->ubicacion .  '</b></h5>';
        return response(array('status'  =>  'success',  'data'  =>  $modal));
    }

    public function delete_zone(Request $request)
    {
        //Data Inputs
        $zone_id  =   $request->input('zone_id');

        if(empty($zone_id))
            return response(array('status'  =>  'error',    'type'  =>  'Imposible eliminar esta Zona'));
        
        $zone_to_be_deleted   =   ZonesModel::find($zone_id);
        $are_there_bars_linked_to_this_zone =   BarsModel::where('zone',$zone_to_be_deleted->id)->first();
        if($are_there_bars_linked_to_this_zone)
            return response(array('status'  =>  'error',    'type'  =>  'Imposible eliminar esta Zona, existen Establecimientos vinculados a ella'));
        
        $zone_to_be_deleted->delete();
        
        return response(array('status'  =>  'success'));
    }

    public function edit($id = null)
    {
        $zone               =   ZonesModel::find($id);
        $zone->ubicacion    =   LocationsModel::find($zone->location)->name;
        $locations          =   LocationsModel::all();
        $modal          =   '<h5>Editando la Zona: <b>' . $zone->name . ' en ' . $zone->ubicacion . '</b></h5>';
        $modal          .=  '<div class="row">
                                <div class="input-field col m6 l6 s12">
                                    <i class="material-icons prefix">local_bar</i>
                                    <input id="name" name="name" type="text" class="validate" value="'.$zone->name.'">
                                    <label for="name">Nombre de la Zona</label>
                                </div>  
                                <div class="input-field col m6 l6 s12">
                                    <i class="material-icons prefix">landscape</i>
                                    <select name="location" id="location">
                                        <option value="">Ubicación de la zona?</option>';
                                        foreach($locations as $location)
                                        {
                                            if($location->id == $zone->location)
                                                $modal.='<option value="'.$location->id.'" selected>'.$location->name.'</option>';
                                            else
                                                $modal.='<option value="'.$location->id.'">'.$location->name.'</option>';
                                        }
                                    $modal.='</select>
                                    <label for="location">Ubicación</label>
                                </div>   
                            </div>';
        return response(array('status'  =>  'success',  'data'  =>  $modal));
    }

    public function save(Request $request)
    {
        //Data Inputs
        $name               =   $request->input('name');
        $location           =   $request->input('location');
        $session_id         =   $request->input('session_id');
        $session_token      =   $request->input('session_token');
        $zone_id            =   $request->input('zone_id');

        if(empty($name))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Nombre de la Zona'));
        if(empty($location))
            return  response(array('status' =>  'error',    'type'  =>  'Falto la Ubicación de la Zona'));

        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $zone    =   ZonesModel::find($zone_id);
        if(strcasecmp($zone->name,$name)!=0)
        {
            $name_already_exists    =   ZonesModel::where('name',$name)->where('location',$location)->first();
            if($name_already_exists)
                return  response(array('status' =>  'error',    'type'  =>  'Este Nombre ya esta asignado a una Zona con esta Ubicación'));
        }else
        {
            $name_already_exists    =   ZonesModel::where('name',$name)->where('location',$location)->first();
            if($name_already_exists)
                return  response(array('status' =>  'error',    'type'  =>  'Este Nombre ya esta asignado a una Zona con esta Ubicación'));
        
        }
        
        $zone->name        =   ucwords(mb_strtolower ($name));
        $zone->location    =   $location;
        $zone->save();

        return response(array('status'  =>  'success'));
    }
}