@extends("partials.layouts.backadmin_users")



@section("content")

<!--

  Content Section Start

-->

<main>

  <container>

    <ul id="slide-out" class="sidenav sidenav-fixed nav" style="top:64px; background-color:#343a40">

      @if(Auth::check() && Auth::user()->user_type == 1)

        <li><a href="{{url('inicio')}}" style="color:#fff"><i class="material-icons" style="color:#fff">home</i>Inicio</a></li>

        <li><a href="{{url('premios')}}" style="color:#fff"><i class="material-icons" style="color:#fff">card_giftcard</i>Premios</a></li>

        <li><a href="{{url('participantes')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Participantes</a></li>

        <li><a href="{{url('establecimientos')}}" style="color:#fff"><i class="material-icons" style="color:#fff">local_bar</i>Establecimientos</a></li>        

        <!--li><a href="{{url('zonas')}}" style="color:#fff"><i class="material-icons" style="color:#fff">landscape</i>Zonas</a></li-->      

        <li><a href="{{url('usuarios')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Usuarios</a></li>        

        <li><a id="btnLogout2"style="color:#fff"><i class="material-icons" style="color:#fff">power_settings_new</i>Cerrar Sesión</a></li>        

      @else

        <li><a href="{{url('participantes')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Participantes</a></li>

        <li><a href="{{url('establecimientos')}}" style="color:#fff"><i class="material-icons" style="color:#fff">local_bar</i>Establecimientos</a></li>        

        <li><a id="btnLogout2"style="color:#fff"><i class="material-icons" style="color:#fff">power_settings_new</i>Cerrar Sesión</a></li>        

      @endif

    </ul>

    <div class="row">

      <div class="col xl9 offset-xl3 l8 offset-l4 m12 s12">

          <h3 class="title">Usuarios</h3>        

      </div>  

      <div class="col xl9 offset-xl3 l8 offset-l4 m12 s12">

        <div class="card blue-white darken-1">

          <div class="card-content black-text">

            <div class="row">

              <div class="col s12" style="overflow-x:auto;">

                <table id="orders" class="striped">

                <thead>

                    <tr>

                        <th>Nombre</th>

                        <th>Correo electrónico</th>

                        <th>Tipo de usuario</th>

                        <th>Pertenece a la isla</th>

                        <th>Acciones</th>

                    </tr>

                </thead>

                <tbody>

                  @foreach($users as $user)

                    <tr>

                      <td>{{$user->name}}</td>

                      <td>{{$user->email}}</td>

                      <td>{{$user->type}}</td>

                      <td>@if($user->location) {{$user->located->name}}@else No tiene isla asociada @endif </td>
                      <td>

                          <a class="editUser small material-icons tooltipped" data-user_id="{{$user->id}}" data-position="bottom" data-delay="50" data-tooltip="Editar"><i class="small material-icons">mode_edit</i></a>

                          <a class="deleteUser small material-icons tooltipped" data-user_id="{{$user->id}}" data-position="bottom" data-delay="50" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a>

                      </td>  

                    </tr>

                  @endforeach

                </tbody>

                </table>

              </div>

            </div>

          </div>

        </div>    

      </div>            

    </div>   

    

    <div class="row">

      <div class="col l4 push-l4 m4 push-m2 s6">

        <a id="btnAgregar" class="waves-effect waves-light btn modal-trigger" href="#modal3">Agregar nuevo usuario</a>

      </div>

      <!--div class="col l4 push-l4 m4 push-m2 s6">

        <a id="btnExportar" class="waves-effect waves-light btn">Exportar lista de usuarios</a>

      </div-->

    </div>

  </container>

</main>

<!-- Modal Structure -->

<div id="modal1" class="modal">

  <div class="modal-content">

  </div>

  <div class="modal-footer">

    <div class="row">

      <div class="col s6">

        <a id="btnGuardar" class="modal-action modal-close waves-effect waves-light btn">Save changes</a>

      </div>

      <div class="col s6">

        <a id="btnCancelarEditar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancel changes</a>

      </div>

    </div>

  </div>

</div>



<!-- Modal Structure -->

<div id="modal2" class="modal">

  <div class="modal-content">

    <h4>Delete Order</h4>

    <p>Do you really want to delete this order?</p>

  </div>

  <div class="modal-footer">

    <div class="row">

      <div class="col s6">

        <a id="btnEliminar" class="modal-action modal-close waves-effect waves-light btn">Confirm</a>

      </div>

      <div class="col s6">

        <a id="btnCancelarEliminar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancel</a>

      </div>

    </div>

  </div>

</div>



<!-- Modal Structure -->

<div id="modal3" class="modal">

  <div class="modal-content">

    <h4 class="center">Llena los datos para el nuevo usuario</h4>

    <div class="row">

      <div class="input-field col m6 l6 s12">

        <i class="material-icons prefix">people</i>

        <input id="new_name" name="new_name" type="text" class="validate">

        <label for="new_name">Nombre del usuario</label>

      </div>  

      <div class="input-field col m6 l6 s12">

        <i class="material-icons prefix">mail</i>

        <input id="new_email" name="new_email" type="email" class="validate">

        <label for="new_email">Correo electrónico</label>

      </div>   

    </div>  

    <div class="row">

        <div class="input-field col m6 l6 s12">

            <i class="material-icons prefix">people</i>

            <input id="new_password" name="new_password" type="password" class="validate">

            <label for="new_password">Contraseña</label>

        </div>  

        <div class="input-field col m6 l6 s12">

            <i class="material-icons prefix">accessibility</i>

            <select name="new_type" id="new_type">

            <option value="" disabled selected>Tipo de usuario?</option>

            @foreach($user_types as $user_type)

                <option value="{{$user_type->id}}">{{$user_type->name}}</option>

            @endforeach

            </select>

            <label for="new_type">Tipo de Usuario</label>

      </div>   

    </div>
    <div class="row">

        <div class="input-field col m6 push-m3 l6 push-l3 s12">

            <i class="material-icons prefix">accessibility</i>

            <select name="new_location" id="new_location">

            <option value="" disabled selected>Ubicación del usuario?</option>

            @foreach($locations as $location)

                <option value="{{$location->id}}">{{$location->name}}</option>

            @endforeach

            </select>

            <label for="new_location">Isla</label>

        </div>  
    </div>  

  </div>

  <div class="modal-footer">

    <div class="row">

      <div class="col s4">

        <a id="btnGuardarNuevo" class="modal-action modal-close waves-effect waves-light btn">Guardar</a>

      </div>

      <div class="col s4 push-s2">

        <a id="btnCancelar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>

      </div>

    </div>

  </div>

</div>

<!--

  Content Section End

-->

@endsection