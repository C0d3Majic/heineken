<style>
    body {
        display: flex;
        min-height: 100vh;
        flex-direction: column;
    }
    main {
        flex: 1 0 auto;
    }

    body {
      background: #3d7d3c ;
    }

    footer{
        background-color: #3d7d3c  !important;
    }

    .round {
        border-radius:15px;
        height:150px;
        background-color:#29C942;
    }

   .MainContainer {
  perspective: 1px;
  transform-style: preserve-3d;
  overflow-x: hidden;
  overflow-y: scroll;
}

.ParallaxContainer {
  display: flex;
  flex: 1 0 auto;
  position: relative;
  height: 100vh;
  transform: translateZ(-1px) scale(2);
  z-index: -1;
}

.ContentContainer {
  display: block;
  position: relative;
  background-color: white;
  z-index: 1;
}

    #inputDni{
        margin-top:0%;
        margin-left:9%;
        border:2px solid !important;
        border-bottom:1px solid;
        border-color:#99C942 !important;
        background-color:#fff;
        text-align:center;
        font-size:3vw;
        color:#205527;
        height:3vh;
        width:80%;
    }

    #home{
        margin-top: 88%;
        margin-left: 64%;
        position: absolute;
    }

    a {
        display:block !important;
    }

    #buttonGenerar{
        margin-top:0%;
        margin-left:10%;
        padding-top:0px
        height:3vh;
        background-color:#e3000f;
        text-align:center;
        font-size:3vw;
        color:#fff;
        width:80%;
    }

    #buttonComprobar{
        margin-top:0%;
        margin-left:10%;
        padding-top:0px
        height:3vh;
        background-color:#e3000f;
        text-align:center;
        font-size:3vw;
        color:#fff;
        width:80%;
    }

    @media only screen and (max-width: 1680px) and (max-height: 1050px) {
        #cards {
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -79% !important;
            right: 34% !important;

        }

        #tickets{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -79% !important;
            right: 11% !important;
        }

        #netflix{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -95% !important;
            right: 34% !important;
        }

        #travels{
            wwidth: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -96.5% !important;
            right: 11% !important;
        }

        #botellas{
            width: 36% !important;
            float: none !important;
            position: absolute !important;
            left: 9% !important;
        }

        #texto_header{
            width: 67% !important;
        }

        #lupa{
            width: 52% !important;
            float: right !important;
            position: absolute !important;
            right: 11.1% !important;
            margin-top: 18% !important;
        }

        #texto_subheader{
            width: 56% !important;
            float: none !important;
            position: absolute !important;
            right: 7% !important;
            padding-top: 10px !important;
        }

        #inputs{
            background-color: white;
            border-radius: 20px;
            width: 36% !important;
            float: none !important;
            position: absolute !important;
            right: 18% !important;
            margin-top: 38% !important;
        }

        #footer{
            position: absolute !important;
            text-align: justify !important;
            left: 11% !important;
            bottom: -121% !important;
        }

        #inputDni{
            font-size:20px;
        }

        #buttonComprobar, #buttonGenerar{
            height:40px;
            font-size:25px;
            width:78%;
        }
    }

    @media only screen and (max-width: 1440px) and (max-height: 900px) {
        #cards {
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -65% !important;
            right: 32% !important;
        }

        #tickets{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -65% !important;
            right: 9% !important;
        }

        #netflix{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -80% !important;
            right: 32% !important;

        }

        #travels{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -81% !important;
            right: 9% !important;
        }

        #botellas{
            width: 38% !important;
            float: none !important;
            position: absolute !important;
            left: 9% !important;
        }

        #texto_header{
            width: 67% !important;
        }

        #lupa{
            width: 53% !important;
            float: right !important;
            position: absolute !important;
            right: 9.1% !important;
            margin-top: 20% !important;
        }

        #texto_subheader{
            width: 54% !important;
            float: none !important;
            position: absolute !important;
            right: 7% !important;
            margin-top: 4%;
        }

        #inputs{
            background-color: white;
            border-radius: 20px;
            width: 36% !important;
            float: none !important;
            position: absolute !important;
            right: 17% !important;
            margin-top: 42% !important;
        }

        #footer{
            position: absolute !important;
            text-align: justify !important;
            left: 6% !important;
            bottom: -104% !important;
        }

        #inputDni{
            ont-size:20px;
        }

        #buttonComprobar, #buttonGenerar{
            height:40px;
            font-size:25px;
            width:78%            
            
        }
    }

    @media only screen and (max-width: 1366px) and (max-height: 800px) {
        #cards {
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -75% !important;
            right: 31% !important;
        }

        #tickets{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -75% !important;
            right: 8% !important;
        }

        #netflix{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -97% !important;
            right: 31% !important;
        }

        #travels{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -98% !important;
            right: 8% !important;
        }

        #botellas{
            width: 39% !important;
            float: none !important;
            position: absolute !important;
            left: 8% !important;
        }

        #texto_header{
            width: 67% !important;
        }

        #lupa{
            width: 51% !important;
            float: right !important;
            position: absolute !important;
            right: 8.1% !important;
            margin-top: 17% !important;
        }

        #texto_subheader{
            width: 54% !important;
            float: none !important;
            position: absolute !important;
            right: 4% !important;
            margin: 3%;
        }

        #inputs{
            background-color: white;
            border-radius: 20px;
            width: 36% !important;
            float: none !important;
            position: absolute !important;
            right: 15% !important;
            margin-top: 37% !important;
        }

        #footer{
            position: absolute !important;
            text-align: justify !important;
            left: 3% !important;
            bottom: -122% !important;
        }

        #inputDni{
            ont-size:20px;
        }

        #buttonComprobar, #buttonGenerar{
            height:40px;
            font-size:25px;
        }
    }

    @media only screen and (max-width: 1334px) and (max-height: 750px) {
        #cards {
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -75% !important;
            right: 31% !important;
        }

        #tickets{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -75% !important;
            right: 8% !important;
        }

        #netflix{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -97% !important;
            right: 31% !important;
        }

        #travels{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -98% !important;
            right: 8% !important;
        }

        #botellas{
            width: 39% !important;
            float: none !important;
            position: absolute !important;
            left: 8% !important;
        }

        #texto_header{
            width: 67% !important;
        }

        #lupa{
            width: 51% !important;
            float: right !important;
            position: absolute !important;
            right: 8.1% !important;
            margin-top: 17% !important;
        }

        #texto_subheader{
            width: 54% !important;
            float: none !important;
            position: absolute !important;
            right: 4% !important;
            margin: 3%;
        }

        #inputs{
            background-color: white;
            border-radius: 20px;
            width: 36% !important;
            float: none !important;
            position: absolute !important;
            right: 15% !important;
            margin-top: 37% !important;
        }

        #footer{
            position: absolute !important;
            text-align: justify !important;
            left: 3% !important;
            bottom: -133%!important;
        }

        #inputDni{
            ont-size:20px;
        }

        #buttonComprobar, #buttonGenerar{
            height:40px;
            font-size:25px;
        }
    }

    @media only screen and (max-width: 1280px) and (max-height: 800px) {
        #cards {
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -54% !important;
            right: 33% !important;
        }

        #tickets{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -55% !important;
            right: 11% !important;
        }

        #netflix{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -69% !important;
            right: 33% !important;
        }

        #travels{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -70.5% !important;
            right: 11% !important;
        }

        #botellas{
            width: 35% !important;
            float: none !important;
            position: absolute !important;
            left: 11% !important;
        }

        #texto_header{
            width: 67% !important;
        }

        #lupa{
            width: 48% !important;
            float: right !important;
            position: absolute !important;
            right: 11.1% !important;
            margin-top: 22% !important;
        }

        #texto_subheader{
            width: 51% !important;
            float: none !important;
            position: absolute !important;
            right: 9% !important;
            margin-top: -1%;
        }

        #inputs{
            background-color:white;
            border-radius:20px;
            width: 36% !important;
            float: none !important;
            position: absolute !important;
            right: 16% !important;
            margin-top: 12% !important;
        }

        #footer{
            position: absolute !important;
            text-align: justify !important;
            left: 0% !important;
            bottom: -91% !important;
        }

        #inputDni{
            ont-size:20px;
        }

        #buttonComprobar, #buttonGenerar{
            height:40px;
            font-size:25px;
        }
    }

    @media only screen and (max-width: 1280px) and (max-height: 680px) {
        #cards {
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -82% !important;
            right: 33% !important;
        }

        #tickets{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -82% !important;
            right: 11% !important;
        }

        #netflix{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -99% !important;
            right: 33% !important;
        }

        #travels{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -100.5% !important;
            right: 11% !important;
        }

        #botellas{
            width: 35% !important;
            float: none !important;
            position: absolute !important;
            left: 11% !important;
        }

        #texto_header{
            width: 67% !important;
        }

        #lupa{
            width: 51% !important;
            float: right !important;
            position: absolute !important;
            right: 11.1% !important;
            margin-top: 17% !important;
        }

        #texto_subheader{
            width: 54% !important;
            float: none !important;
            position: absolute !important;
            right: 8% !important;
            margin-top: 3%;
        }

        #inputs{
            background-color: white;
            border-radius: 20px;
            width: 36% !important;
            float: none !important;
            position: absolute !important;
            right: 18% !important;
            margin-top: 37% !important;
        }

        #footer{
            position: absolute !important;
            text-align: justify !important;
            left: 0% !important;
            bottom: -125% !important;
        }

        #inputDni{
            ont-size:20px;
        }

        #buttonComprobar, #buttonGenerar{
            height: 40px;
            font-size: 25px;
            width: 78%;
        }
    }

    @media only screen and (max-width: 1264px) and (max-height: 733px) {
        #cards {
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -71% !important;
            right: 32% !important;
        }

        #tickets{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -71% !important;
            right: 11% !important;
        }

        #netflix{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -86% !important;
            right: 32% !important;
        }

        #travels{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -87% !important;
            right: 11% !important;
        }

        #botellas{
            width: 35% !important;
            float: none !important;
            position: absolute !important;
            left: 12% !important;
        }

        #texto_header{
            width: 73% !important;
        }

        #lupa{
            width: 51% !important;
            float: right !important;
            position: absolute !important;
            right: 10.1% !important;
            margin-top: 16% !important;
        }

        #texto_subheader{
            width: 57% !important;
            float: none !important;
            position: absolute !important;
            right: 1% !important;
            margin-top: 1%;
        }

        #inputs{
            background-color: white;
            border-radius: 20px;
            width: 36% !important;
            float: none !important;
            position: absolute !important;
            right: 17% !important;
            margin-top: 36% !important;

        }

        #footer{
            position: absolute !important;
            text-align: justify !important;
            left: 0% !important;
            bottom: -109% !important;
        }

        #inputDni{
            ont-size:20px;
        }

        #buttonComprobar, #buttonGenerar{
            height:40px;
            font-size:25px;
        }
    }

    @media only screen and (max-width: 1152px) and (max-height: 720px) 
    {
        #cards {
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -66% !important;
            right: 31% !important;
        }

        #tickets{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -66% !important;
            right: 10% !important;
        }

        #netflix{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -82% !important;
            right: 31% !important;
        }

        #travels{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -82.5% !important;
            right: 10% !important;
        }

        #botellas{
            width: 38% !important;
            float: none !important;
            position: absolute !important;
            left: 10% !important;
        }

        #texto_header{
            width: 70% !important;
        }

        #lupa{
            width: 52% !important;
            float: right !important;
            position: absolute !important;
            right: 10.1% !important;
            margin-top: 19% !important;
        }

        #texto_subheader{
            width: 53% !important;
            float: none !important;
            position: absolute !important;
            right: 5% !important;
            padding-top: 2% !important;
        }

        #inputs{
            background-color: white;
            border-radius: 20px;
            width: 36% !important;
            float: none !important;
            position: absolute !important;
            right: 17% !important;
            margin-top: 40% !important;
        }

        #footer{
            position: absolute !important;
            text-align: justify !important;
            left: 0% !important;
            bottom: -107% !important;

        }

        #inputDni{
            ont-size:20px;
        }

        #buttonComprobar, #buttonGenerar{
            height:40px;
            font-size:25px;
        }
    }

    @media only screen and (max-width: 1024px) and (max-height: 840px){
        #cards {
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -25% !important;
            right: 31% !important;
        }

        #tickets{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -25% !important;
            right: 10% !important;
        }

        #netflix{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -40% !important;
            right: 31% !important;
        }

        #travels{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -40.5% !important;
            right: 10% !important;
        }

        #botellas{
            width: 38% !important;
            float: none !important;
            position: absolute !important;
            left: 10% !important;
        }

        #texto_header{
            width: 70% !important;
        }

        #lupa{
            width: 52% !important;
            float: right !important;
            position: absolute !important;
            right: 10.1% !important;
            margin-top: 19% !important;
        }

        #texto_subheader{
            width: 53% !important;
            float: none !important;
            position: absolute !important;
            right: 5% !important;
            padding-top: 2% !important;
        }

        #inputs{
            background-color: white;
            border-radius: 20px;
            width: 36% !important;
            float: none !important;
            position: absolute !important;
            right: 17% !important;
            margin-top: 40% !important;
        }

        #footer{
            position: absolute !important;
            text-align: justify !important;
            left: 0% !important;
            bottom: -65% !important;

        }

        #inputDni{
            ont-size:20px;
        }

        #buttonComprobar, #buttonGenerar{
            height:40px;
            font-size:25px;
        }
    }

    @media only screen and (max-width: 1024px) and (max-height: 768px){
        #cards {
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -54% !important;
            right: 31% !important;
        }

        #tickets{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -54% !important;
            right: 10% !important;
        }

        #netflix{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -75% !important;
            right: 31% !important;
        }

        #travels{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -75.5% !important;
            right: 10% !important;
        }

        #botellas{
            width: 38% !important;
            float: none !important;
            position: absolute !important;
            left: 10% !important;
        }

        #texto_header{
            width: 70% !important;
        }

        #lupa{
            width: 52% !important;
            float: right !important;
            position: absolute !important;
            right: 10.1% !important;
            margin-top: 19% !important;
        }

        #texto_subheader{
            width: 53% !important;
            float: none !important;
            position: absolute !important;
            right: 5% !important;
            padding-top: 2% !important;
        }

        #inputs{
            background-color: white;
            border-radius: 20px;
            width: 36% !important;
            float: none !important;
            position: absolute !important;
            right: 17% !important;
            margin-top: 40% !important;
        }

        #footer{
            position: absolute !important;
            text-align: justify !important;
            left: 0% !important;
            bottom: -100% !important;

        }

        #inputDni{
            ont-size:20px;
        }

        #buttonComprobar, #buttonGenerar{
            height:40px;
            font-size:25px;
        }
    }

    @media only screen and (max-width: 768px) and (max-height: 1024px) {
        #cards {
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: 17% !important;
            right: 33% !important;
        }

        #tickets{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: 17% !important;
            right: 9% !important;
        }

        #netflix{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: 6% !important;
            right: 33% !important;
        }

        #travels{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: 5.7% !important;
            right: 9% !important;

        }

        #botellas{
            width: 37% !important;
            float: none !important;
            position: absolute !important;
            left: 8% !important;
        }

        #texto_header{
            width: 70% !important;
        }

        #lupa{
            width: 56% !important;
            float: right !important;
            position: absolute !important;
            right: 8.1% !important;
            margin-top: 14% !important;
        }

        #texto_subheader{
            width: 56% !important;
            float: none !important;
            position: absolute !important;
            right: 7% !important;
            padding-top: 3% !important;
        }

        #inputs{
            background-color: white;
            border-radius: 20px;
            width: 39% !important;
            float: none !important;
            position: absolute !important;
            right: 15% !important;
            margin-top: 33% !important;
        }

        #footer{
            position: absolute !important;
            text-align: justify !important;
            left: 0% !important;
            bottom: -14% !important;
        }

        #inputDni{
            font-size:20px;
            width: 77%;
        }

        #buttonComprobar, #buttonGenerar{
            height: 25px;
            font-size: 18px;

        }
    }
    
    @media only screen and (max-width:767px) {
        #botellas{
            width: 84% !important;
            float: none !important;
            position: absolute !important;
            left: 8% !important;
        }

        #texto_header{
            width: 90% !important;
            left: 5%;
            position:absolute;
            z-index:1;
        }

        #responsive{
            width: 85% !important;
            float: right !important;
            position: absolute !important;
            right: 8.1% !important;
            margin-bottom: -188% !important;
        }

        #lupa{
            width: 85% !important;
            float: right !important;
            position: absolute !important;
            right: 9.1% !important;
            margin-top: -89% !important;
        }

        #inputs{
            background-color: white;
            border-radius: 20px;
            width: 39% !important;
            float: none !important;
            position: absolute !important;
            right: 30% !important;
            margin-top: -62% !important;
        }

        #texto_subheader{
            width: 79% !important;
            float: none !important;
            position: absolute !important;
            right: 7% !important;
            padding-top: 26% !important;
            margin-top: -143%;
        }

        #footer{
            position: absolute !important;
            text-align: justify !important;
            left: 0% !important;
            bottom: -21% !important;
        }

    }
    /*
    @media only screen and (max-width: 736px) and (max-height: 414px) {
        #cards {
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -106% !important;
            right: 33% !important;
        }

        #tickets{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -106% !important;
            right: 9% !important;
        }

        #netflix{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -133% !important;
            right: 33% !important;
        }

        #travels{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -134.3% !important;
            right: 9% !important;

        }

        #botellas{
            width: 37% !important;
            float: none !important;
            position: absolute !important;
            left: 8% !important;
        }

        #texto_header{
            width: 70% !important;
        }

        #lupa{
            width: 56% !important;
            float: right !important;
            position: absolute !important;
            right: 8.1% !important;
            margin-top: 14% !important;
        }

        #texto_subheader{
            width: 56% !important;
            float: none !important;
            position: absolute !important;
            right: 7% !important;
            padding-top: 3% !important;
        }

        #inputs{
            background-color: white;
            border-radius: 20px;
            width: 39% !important;
            float: none !important;
            position: absolute !important;
            right: 15% !important;
            margin-top: 33% !important;
        }

        #footer{
            position: absolute !important;
            text-align: justify !important;
            left: 0% !important;
            bottom: -169% !important;
        }

        #inputDni{
            font-size:20px;
            width: 77%;
        }

        #buttonComprobar, #buttonGenerar{
            height: 25px;
            font-size: 18px;

        }
    }

    @media only screen and (max-width: 734px) and (max-height: 375px) {
        #cards {
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -100% !important;
            right: 33% !important;
        }

        #tickets{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -100% !important;
            right: 9% !important;
        }

        #netflix{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -122% !important;
            right: 33% !important;
        }

        #travels{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -122.3% !important;
            right: 9% !important;

        }

        #botellas{
            width: 37% !important;
            float: none !important;
            position: absolute !important;
            left: 8% !important;
        }

        #texto_header{
            width: 70% !important;
        }

        #lupa{
            width: 56% !important;
            float: right !important;
            position: absolute !important;
            right: 8.1% !important;
            margin-top: 14% !important;
        }

        #texto_subheader{
            width: 56% !important;
            float: none !important;
            position: absolute !important;
            right: 7% !important;
            padding-top: 3% !important;
        }

        #inputs{
            background-color: white;
            border-radius: 20px;
            width: 39% !important;
            float: none !important;
            position: absolute !important;
            right: 15% !important;
            margin-top: 33% !important;
        }

        #footer{
            position: absolute !important;
            text-align: justify !important;
            left: 0% !important;
            bottom: -156% !important;
        }

        #inputDni{
            font-size:20px;
            width: 77%;
        }

        #buttonComprobar, #buttonGenerar{
            height: 25px;
            font-size: 18px;

        }
    }

    @media only screen and (max-width: 684px) and (max-height: 412px) {
        #cards {
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -79% !important;
            right: 33% !important;
        }

        #tickets{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -79% !important;
            right: 9% !important;
        }

        #netflix{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -100% !important;
            right: 33% !important;
        }

        #travels{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -101.3% !important;
            right: 9% !important;

        }

        #botellas{
            width: 37% !important;
            float: none !important;
            position: absolute !important;
            left: 8% !important;
        }

        #texto_header{
            width: 70% !important;
        }

        #lupa{
            width: 56% !important;
            float: right !important;
            position: absolute !important;
            right: 8.1% !important;
            margin-top: 14% !important;
        }

        #texto_subheader{
            width: 56% !important;
            float: none !important;
            position: absolute !important;
            right: 7% !important;
            padding-top: 3% !important;
        }

        #inputs{
            background-color: white;
            border-radius: 20px;
            width: 39% !important;
            float: none !important;
            position: absolute !important;
            right: 15% !important;
            margin-top: 33% !important;
        }

        #footer{
            position: absolute !important;
            text-align: justify !important;
            left: 0% !important;
            bottom: -133% !important;
        }

        #inputDni{
            font-size:20px;
            width: 77%;
        }

        #buttonComprobar, #buttonGenerar{
            height: 25px;
            font-size: 18px;

        }
    }

    @media only screen and (max-width: 667px) and (max-height: 375px) {
        #cards {
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -110% !important;
            right: 33% !important;
        }

        #tickets{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -110% !important;
            right: 9% !important;
        }

        #netflix{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -139% !important;
            right: 33% !important;
        }

        #travels{
            width: 20% !important;
            float: right !important;
            position: none !important;
            bottom: -140.3% !important;
            right: 9% !important;

        }

        #botellas{
            width: 37% !important;
            float: none !important;
            position: absolute !important;
            left: 8% !important;
        }

        #texto_header{
            width: 70% !important;
        }

        #lupa{
            width: 56% !important;
            float: right !important;
            position: absolute !important;
            right: 8.1% !important;
            margin-top: 14% !important;
        }

        #texto_subheader{
            width: 56% !important;
            float: none !important;
            position: absolute !important;
            right: 7% !important;
            padding-top: 3% !important;
        }

        #inputs{
            background-color: white;
            border-radius: 20px;
            width: 39% !important;
            float: none !important;
            position: absolute !important;
            right: 15% !important;
            margin-top: 33% !important;
        }

        #footer{
            position: absolute !important;
            text-align: justify !important;
            left: 0% !important;
            bottom: -176% !important;
        }

        #inputDni{
            font-size:20px;
            width: 77%;
        }

        #buttonComprobar, #buttonGenerar{
            height: 25px;
            font-size: 18px;

        }
    }
    @media only screen  and (max-width : 414px)  and (max-height : 736px) { 
        #cards {
                width: 100% !important;
                float: right !important;
                position: none !important;
                bottom: -55% !important;
                right: 0% !important;
            }

            #tickets{
                width: 100% !important;
                float: right !important;
                position: none !important;
                bottom: -77% !important;
                right: 0% !important;
            }

            #netflix{
                width: 100% !important;
                float: right !important;
                position: none !important;
                bottom: -97% !important;
                right: 0% !important;
            }

            #travels{
                width: 100% !important;
                float: right !important;
                position: none !important;
                bottom: -121.3% !important;
                right: 0% !important;
            }

            #botellas{
                width: 70% !important;
                float: none !important;
                position: absolute !important;
                left: 17% !important;
            }

            #texto_header{
                width: 90% !important;
            }

            #lupa{
                width: 90% !important;
                float: right !important;
                position: absolute !important;
                right: 5.1% !important;
                margin-top: 60% !important;
            }

            #texto_subheader{
                width: 100% !important;
                float: none !important;
                position: absolute !important;
                right: -3% !important;
                padding-top: 0% !important;
            }

            #inputs{
                background-color: white;
                border-radius: 20px;
                width: 55% !important;
                float: none !important;
                position: absolute !important;
                right: 20% !important;
                margin-top: 87% !important;
            }

            #footer{
                position: absolute !important;
                text-align: justify !important;
                left: 0% !important;
                bottom: -140% !important;
            }

            #inputDni{
                font-size:20px;
            }

            #buttonComprobar, #buttonGenerar{
                height: 35px;
                padding-top: 5px;
                font-size: 15px;
                width:76%;
            }
    }

    @media only screen  and (max-width : 412px)  and (max-height : 684px) { 
        #cards {
                width: 100% !important;
                float: right !important;
                position: none !important;
                bottom: -55% !important;
                right: 0% !important;
            }

            #tickets{
                width: 100% !important;
                float: right !important;
                position: none !important;
                bottom: -77% !important;
                right: 0% !important;
            }

            #netflix{
                width: 100% !important;
                float: right !important;
                position: none !important;
                bottom: -97% !important;
                right: 0% !important;
            }

            #travels{
                width: 100% !important;
                float: right !important;
                position: none !important;
                bottom: -121.3% !important;
                right: 0% !important;
            }

            #botellas{
                width: 70% !important;
                float: none !important;
                position: absolute !important;
                left: 17% !important;
            }

            #texto_header{
                width: 90% !important;
            }

            #lupa{
                width: 90% !important;
                float: right !important;
                position: absolute !important;
                right: 5.1% !important;
                margin-top: 60% !important;
            }

            #texto_subheader{
                width: 100% !important;
                float: none !important;
                position: absolute !important;
                right: -3% !important;
                padding-top: 0% !important;
            }

            #inputs{
                background-color: white;
                border-radius: 20px;
                width: 55% !important;
                float: none !important;
                position: absolute !important;
                right: 20% !important;
                margin-top: 87% !important;
            }

            #footer{
                position: absolute !important;
                text-align: justify !important;
                left: 0% !important;
                bottom: -140% !important;
            }

            #inputDni{
                font-size:20px;
            }

            #buttonComprobar, #buttonGenerar{
                height: 35px;
                padding-top: 5px;
                font-size: 15px;
                width:76%;
            }
    }

    @media only screen and (max-width : 375px) and (max-height : 812px) { 
        #cards {
                width: 100% !important;
                float: right !important;
                position: none !important;
                bottom: -33% !important;
                right: 0% !important;
            }

            #tickets{
                width: 100% !important;
                float: right !important;
                position: none !important;
                bottom: -53% !important;
                right: 0% !important;
            }

            #netflix{
                width: 100% !important;
                float: right !important;
                position: none !important;
                bottom: -72% !important;
                right: 0% !important;
            }

            #travels{
                width: 100% !important;
                float: right !important;
                position: none !important;
                bottom: -94.3% !important;
                right: 0% !important;
            }

            #botellas{
                width: 70% !important;
                float: none !important;
                position: absolute !important;
                left: 17% !important;
            }

            #texto_header{
                width: 90% !important;
            }

            #lupa{
                width: 90% !important;
                float: right !important;
                position: absolute !important;
                right: 5.1% !important;
                margin-top: 60% !important;
            }

            #texto_subheader{
                width: 100% !important;
                float: none !important;
                position: absolute !important;
                right: -3% !important;
                padding-top: 0% !important;
            }

            #inputs{
                background-color: white;
                border-radius: 20px;
                width: 55% !important;
                float: none !important;
                position: absolute !important;
                right: 20% !important;
                margin-top: 87% !important;
            }

            #footer{
                position: absolute !important;
                text-align: justify !important;
                left: 0% !important;
                bottom: -111% !important;
            }

            #inputDni{
                font-size:20px;
            }

            #buttonComprobar, #buttonGenerar{
                height: 35px;
                padding-top: 5px;
                font-size: 15px;
                width:76%;
            }
    }
    @media only screen  and (max-width: 375px)  and (max-height: 667px){
        #cards {
                width: 100% !important;
                float: right !important;
                position: none !important;
                bottom: -55% !important;
                right: 0% !important;
            }

            #tickets{
                width: 100% !important;
                float: right !important;
                position: none !important;
                bottom: -77% !important;
                right: 0% !important;
            }

            #netflix{
                width: 100% !important;
                float: right !important;
                position: none !important;
                bottom: -97% !important;
                right: 0% !important;
            }

            #travels{
                width: 100% !important;
                float: right !important;
                position: none !important;
                bottom: -120.3% !important;
                right: 0% !important;
            }

            #botellas{
                width: 70% !important;
                float: none !important;
                position: absolute !important;
                left: 17% !important;
            }

            #texto_header{
                width: 90% !important;
            }

            #lupa{
                width: 90% !important;
                float: right !important;
                position: absolute !important;
                right: 5.1% !important;
                margin-top: 60% !important;
            }

            #texto_subheader{
                width: 100% !important;
                float: none !important;
                position: absolute !important;
                right: -3% !important;
                padding-top: 0% !important;
            }

            #inputs{
                background-color: white;
                border-radius: 20px;
                width: 55% !important;
                float: none !important;
                position: absolute !important;
                right: 20% !important;
                margin-top: 87% !important;
            }

            #footer{
                position: absolute !important;
                text-align: justify !important;
                left: 0% !important;
                bottom: -140% !important;
            }

            #inputDni{
                font-size:20px;
            }

            #buttonComprobar, #buttonGenerar{
                height: 35px;
                padding-top: 5px;
                font-size: 15px;
                width:76%;
            }
    }*/`

</style>