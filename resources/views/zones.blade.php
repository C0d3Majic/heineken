@extends("partials.layouts.backadmin_zones")

@section("content")
<!--
  Content Section Start
-->
<main>
  <container>
    <ul id="slide-out" class="sidenav sidenav-fixed nav" style="top:64px; background-color:#343a40">
      @if(Auth::check() && Auth::user()->user_type == 1)
        <li><a href="{{url('inicio')}}" style="color:#fff"><i class="material-icons" style="color:#fff">home</i>Inicio</a></li>
        <li><a href="{{url('premios')}}" style="color:#fff"><i class="material-icons" style="color:#fff">card_giftcard</i>Premios</a></li>
        <li><a href="{{url('participantes')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Participantes</a></li>
        <li><a href="{{url('establecimientos')}}" style="color:#fff"><i class="material-icons" style="color:#fff">local_bar</i>Establecimientos</a></li>        
        <!--li><a href="{{url('zonas')}}" style="color:#fff"><i class="material-icons" style="color:#fff">landscape</i>Zonas</a></li-->      
        <li><a href="{{url('usuarios')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Usuarios</a></li>        
        <li><a id="btnLogout2"style="color:#fff"><i class="material-icons" style="color:#fff">power_settings_new</i>Cerrar Sesión</a></li>        
      @else
        <li><a href="{{url('participantes')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Participantes</a></li>
        <li><a href="{{url('establecimientos')}}" style="color:#fff"><i class="material-icons" style="color:#fff">local_bar</i>Establecimientos</a></li>        
        <li><a id="btnLogout2"style="color:#fff"><i class="material-icons" style="color:#fff">power_settings_new</i>Cerrar Sesión</a></li>        
      @endif
    </ul>
    <div class="row">
      <div class="col l9 offset-l3 m12 s12">
          <h3 class="title">Zonas</h3>        
      </div>  
      <div class="col l9 offset-l3 m12 s12">
        <div class="card blue-white darken-1">
          <div class="card-content black-text">
            <div class="row">
              <div class="col s12">
                <table id="orders" class="responsive-table">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Ubicación</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($zones as $zone)
                    <tr>
                      <td>{{$zone->name}}</td>
                      <td>{{$zone->location}}</td>
                      <td>
                          <a class="editZone small material-icons tooltipped" data-zone_id="{{$zone->id}}" data-position="bottom" data-delay="50" data-tooltip="Editar"><i class="small material-icons">mode_edit</i></a>
                          <a class="deleteZone small material-icons tooltipped" data-zone_id="{{$zone->id}}" data-position="bottom" data-delay="50" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a>
                      </td>  
                    </tr>
                  @endforeach
                </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>    
      </div>            
    </div>   
    
    <div class="row">
      <div class="col l4 push-l4 m4 push-m2 s6">
        <a id="btnAgregar" class="waves-effect waves-light btn modal-trigger" href="#modal3">Agregar nueva zona</a>
      </div>
      <div class="col l4 push-l4 m4 push-m2 s6">
        <a id="btnExportar" class="waves-effect waves-light btn">Exportar lista de zonas</a>
      </div>
    </div>
  </container>
</main>
<!-- Modal Structure -->
<div id="modal1" class="modal">
  <div class="modal-content">
  </div>
  <div class="modal-footer">
    <div class="row">
      <div class="col s6">
        <a id="btnGuardar" class="modal-action modal-close waves-effect waves-light btn">Save changes</a>
      </div>
      <div class="col s6">
        <a id="btnCancelarEditar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancel changes</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal Structure -->
<div id="modal2" class="modal">
  <div class="modal-content">
    <h4>Delete Order</h4>
    <p>Do you really want to delete this order?</p>
  </div>
  <div class="modal-footer">
    <div class="row">
      <div class="col s6">
        <a id="btnEliminar" class="modal-action modal-close waves-effect waves-light btn">Confirm</a>
      </div>
      <div class="col s6">
        <a id="btnCancelarEliminar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancel</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal Structure -->
<div id="modal3" class="modal">
  <div class="modal-content">
    <h4 class="center">Llena los datos para la nueva zona</h4>
    <div class="row">
      <div class="input-field col m6 l6 s12">
        <i class="material-icons prefix">landscape</i>
        <input id="new_name" name="new_name" type="text" class="validate">
        <label for="new_name">Nombre de la Zona</label>
      </div>  
      <div class="input-field col m6 l6 s12">
        <i class="material-icons prefix">landscape</i>
        <select name="new_location" id="new_location">
          <option value="" disabled selected>Ubicación de la zona?</option>
          @foreach($locations as $location)
            <option value="{{$location->id}}">{{$location->name}}</option>
          @endforeach
        </select>
        <label for="new_location">Ubicación</label>
      </div>   
    </div>  
  </div>  
  <div class="modal-footer">
    <div class="row">
      <div class="col s4">
        <a id="btnGuardarNuevo" class="modal-action modal-close waves-effect waves-light btn">Guardar</a>
      </div>
      <div class="col s4 push-s2">
        <a id="btnCancelar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
      </div>
    </div>
  </div>
</div>
<!--
  Content Section End
-->
@endsection