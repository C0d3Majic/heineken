<script>
    $(document).ready(function(){
        M.updateTextFields();
        $("#buttonLogin").click(function(){
             // Start $.ajax() method
             $.ajax({
                // The URL for the request. variable set above
                url: $("#login_form").attr('action'),
                // The data to send (will be converted to a query string). variable set above
                data: $("#login_form").serialize(),
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html:$toastContent, classes: 'rounded red', displayLength:2000});
                        Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                    }else if(msg.status == 'success'){
                        M.toast({html: 'Bienvenido!', classes: 'rounded', displayLength:2000, completeCallback: function(){window.location.reload()}});                    
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
            return false;
        })
        
        
        $("#buttonRecuperar").click(function(){
            if($("#email").val().length>0){
                M.toast({html: 'Revisa tu bandeja de entrada para recuperar tu contraseña!', classes: 'rounded', displayLength:3000});
            }else
            {
                M.toast({html: 'Oops se te olvido darnos tu email!', classes: 'rounded red', displayLength:3000});
            }
        })        
    });
</script>