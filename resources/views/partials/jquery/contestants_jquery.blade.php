<script>
    document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.tooltipped');
            var instances = M.Tooltip.init(elems, {inDuration:100,outDuration:100});
            instances.forEach(function(element){
                console.log(element)                
            })
    });
    
    $(document).ready(function(){

        $('.modal').modal({dismissible:false});        

        $('#orders').DataTable({responsive: true});

        $('.sidenav').sidenav();

        //$('.tooltipped').tooltip();
        
        $(".dropdown-trigger").dropdown({

            hover:true,

            coverTrigger:false

        });

        $('select').formSelect({hover:false});

        var contestant_id;



         $('body').on('click', '.editContestant', function(){

            contestant_id = $(this).data('contestant_id');

            $.ajax({

                // The URL for the request. variable set above

                url: "{{url('edit/contestant')}}/"+contestant_id,

                // Whether this is a POST or GET request

                type: "GET",

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

                // The type of data we expect back. can be json, html, text, etc...

                dataType : "json",

                // Code to run if the request succeeds;

                // the response is passed to the function

                success: function (response) {  

                if(response.status == 'error'){

                    var $toastContent = $('<span>'+ response.type +'</span>');

                    M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});

                }else if(response.status == 'success'){

                    $('.modal-content').html(response.data);

                    $('select').formSelect();

                    M.updateTextFields();

                    $('#modal1').modal('open');

                }

                },

                error: function(){

                //window.location.reload();

                var $toastContent = $('<span>Hubo un error en el servidor</span>');

                M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});

                }

            });

        });



        $('body').on('click', '.deleteContestant', function(){

            contestant_id = $(this).data('contestant_id');

            $.ajax({

                // The URL for the request. variable set above

                url: "{{url('delete/contestant')}}/"+contestant_id,

                // Whether this is a POST or GET request

                type: "GET",

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

                // The type of data we expect back. can be json, html, text, etc...

                dataType : "json",

                // Code to run if the request succeeds;

                // the response is passed to the function

                success: function (response) {   

                    if(response.status == 'error'){

                        var $toastContent = $('<span>'+ response.type +'</span>');

                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});

                    }else if(response.status == 'success'){

                        $('.modal-content').html(response.data);

                        $('select').formSelect();

                        M.updateTextFields();

                        $('#modal2').modal('open');

                    }

                },

                error: function(){

                //window.location.reload();

                    var $toastContent = $('<span>Hubo un error en el servidor</span>');

                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});

                }

            });

        });



        $('#btnCancelarEditar').click(function(){

            M.toast({html: 'Edicion Cancelada!', classes: 'rounded', displayLength:1000, completeCallback: function(){window.location.reload()}});                    

        });



        $('#btnCancelarEliminar').click(function(){

            M.toast({html: 'Eliminacion Cancelada!', classes: 'rounded', displayLength:1000, completeCallback: function(){window.location.reload()}});                    

        });

        $("#btnExportar").click(function(){            
            var jsonObject = {
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}"
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('export/contestants')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:1000, completeCallback: function(){window.location.reload()}});
                    }else{
                        M.toast({html: 'Exportacion Exitosa!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.open("{{url('/')}}/participantes.xlsx", '_new')}}); 
                        
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });



        $('#btnEliminar').click(function(){

            var jsonObject = {

                session_id: "{{$session_id}}",

                session_token : "{{$session_token}}",

                contestant_id : contestant_id

            }

            $.ajax({

                // The URL for the request. variable set above

                url: "{{url('delete/contestant')}}",

                // The data to send (will be converted to a query string). variable set above

                data: jsonObject,

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

                // Whether this is a POST or GET request

                type: "POST",

                // The type of data we expect back. can be json, html, text, etc...

                dataType : "json",

                // Code to run if the request succeeds;

                // the response is passed to the function

                success: function( msg ) {

                    if(msg.status == 'error'){

                        var $toastContent = $('<span>'+ msg.type +'</span>');

                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000, completeCallback: function(){window.location.reload()}});

                    }else{

                        M.toast({html: 'Participante Eliminado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 

                    }

                },

                error: function(){

                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});

                }

            });

        })

        $('#btnLogout2').click(function(){

            // Start $.ajax() method

            var jsonObject = {

                session_id: "{{$session_id}}",

                session_token : "{{$session_token}}"

            }

            $.ajax({

            // The URL for the request. variable set above

            url: "{{url('user/logout')}}",

            // The data to send (will be converted to a query string). variable set above

            data: jsonObject,

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            },

            // Whether this is a POST or GET request

            type: "POST",

            // The type of data we expect back. can be json, html, text, etc...

            dataType : "json",

            // Code to run if the request succeeds;

            // the response is passed to the function

            success: function( msg ) {

                if(msg.status == 'error'){

                var $toastContent = $('<span>'+ msg.type +'</span>');

                M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});

                }else{

                window.location.reload();

                }

            },

            error: function(){

                M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});

            }

            });

        }); 

        $('#btnLogout').click(function(){

            // Start $.ajax() method

            var jsonObject = {

                session_id: "{{$session_id}}",

                session_token : "{{$session_token}}"

            }

            $.ajax({

                // The URL for the request. variable set above

                url: "{{url('user/logout')}}",

                // The data to send (will be converted to a query string). variable set above

                data: jsonObject,

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

                // Whether this is a POST or GET request

                type: "POST",

                // The type of data we expect back. can be json, html, text, etc...

                dataType : "json",

                // Code to run if the request succeeds;

                // the response is passed to the function

                success: function( msg ) {

                    if(msg.status == 'error'){

                        var $toastContent = $('<span>'+ msg.type +'</span>');

                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});

                    }else{

                        window.location.reload();

                    }

                },

                error: function(){

                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});

                }

            });

        });  



        var bases_checked = 0;

        $('#bases').change(function(){

            bases_checked = this.checked ? 1 : 0;

        });

        var acepto_participar_checked = 0;

        $('#acepto_participar').change(function(){

            acepto_participar_checked = this.checked ? 1 : 0;

        });

        var acepto_email_checked = 0;

        $('#acepto_email').change(function(){

            acepto_email_checked = this.checked ? 1 : 0;

        });

        var acepto_sorteo_checked = 0;

        $('#acepto_sorteo').change(function(){

            acepto_sorteo_checked = this.checked ? 1 : 0;

        });



        $("#btnGuardarNuevo").click(function(){

             // Start $.ajax() method

             var jsonObject = {

                session_id      :   "{{$session_id}}",

                session_token   :   "{{$session_token}}",

                name            :   $("#new_name").val(),

                lastname        :   $("#new_lastname").val(),

                dni             :   $("#new_dni").val(),

                email           :   $("#new_email").val(),

                bar             :   $("#new_bar").val(),

                location        :   $("#new_location").val(),

                bases           :   bases_checked,

                acepto_participar:  acepto_participar_checked,

                acepto_email    :   acepto_email_checked,

                acepto_sorteo   :   acepto_sorteo_checked

            }

            $.ajax({

                // The URL for the request. variable set above

                url: "{{url('new/contestant')}}",

                // The data to send (will be converted to a query string). variable set above

                data: jsonObject,

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

                // Whether this is a POST or GET request

                type: "POST",

                // The type of data we expect back. can be json, html, text, etc...

                dataType : "json",

                // Code to run if the request succeeds;

                // the response is passed to the function

                success: function( msg ) {

                    if(msg.status == 'error'){

                        var $toastContent = $('<span>'+ msg.type +'</span>');

                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000, completeCallback: function(){}});

                    }else{

                        M.toast({html: 'Participante Agregado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 

                    }

                },

                error: function(){

                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});

                }

            });

        });



        $("#btnGuardar").click(function(){

            // Start $.ajax() method

            var jsonObject = {

                session_id      :   "{{$session_id}}",

                session_token   :   "{{$session_token}}",

                name            :   $("#name").val(),

                lastname        :   $("#lastname").val(),

                dni             :   $("#dni").val(),

                email           :   $("#email").val(),

                bar             :   $("#bar").val(),

                location        :   $("#location").val(),

                contestant_id : contestant_id

            }

            $.ajax({

                // The URL for the request. variable set above

                url: "{{url('save/contestant')}}",

                // The data to send (will be converted to a query string). variable set above

                data: jsonObject,

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

                // Whether this is a POST or GET request

                type: "POST",

                // The type of data we expect back. can be json, html, text, etc...

                dataType : "json",

                // Code to run if the request succeeds;

                // the response is passed to the function

                success: function( msg ) {

                    if(msg.status == 'error'){

                        var $toastContent = $('<span>'+ msg.type +'</span>');

                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000, completeCallback: function(){window.location.reload()}});

                    }else{

                        M.toast({html: 'Participante Editado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 

                    }

                },

                error: function(){

                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});

                }

            });

        })

    });

</script>