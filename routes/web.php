<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',                 'SiteController@redirect_landing');
Route::get('agradecimiento',    'SiteController@show_view_agradecimiento');
Route::get('login',             'SiteController@show_view_login');
Route::get('inicio',            'SiteController@show_view_dashboard');
Route::get('premios',           'SiteController@show_view_prizes');
Route::get('participantes',      'SiteController@show_view_contestants');
Route::get('recuperar',         'SiteController@show_view_recuperar');
Route::get('establecimientos',  'SiteController@show_view_bars');
Route::get('zonas',             'SiteController@show_view_zones');
Route::get('usuarios',          'SiteController@show_view_users');
Route::get('bases',             'SiteController@show_view_bases');

Route::post('user/login',       'UsersController@login');
Route::post('user/logout',      'UsersController@logout');

Route::post('new/contestant',           'ContestantsController@new');
Route::get('edit/contestant/{id}',      'ContestantsController@edit');
Route::get('delete/contestant/{id}',    'ContestantsController@delete');
Route::post('delete/contestant',        'ContestantsController@delete_contestant');
Route::post('save/contestant',          'ContestantsController@save');
Route::post('export/contestants',       'ContestantsController@export');

Route::post('new/prize',        'PrizesController@new');
Route::get('edit/prize/{id}',   'PrizesController@edit');
Route::get('delete/prize/{id}', 'PrizesController@delete');
Route::post('delete/prize',     'PrizesController@delete_prize');
Route::post('save/prize',       'PrizesController@save');
Route::post('export/prizes',    'PrizesController@export');
Route::post('valid/dni',        'PrizesController@validate_dni');
Route::post('valid/code',       'PrizesController@validate_code');


Route::post('new/bar',          'BarsController@new');
Route::get('edit/bar/{id}',     'BarsController@edit');
Route::get('delete/bar/{id}',   'BarsController@delete');
Route::post('save/bar',         'BarsController@save');
Route::post('delete/bar',       'BarsController@delete_bar');
Route::post('export/bars',      'BarsController@export');

Route::get('new/contestant/bar/{id}','BarsController@new_contestant');
Route::post('save/new/participant/bar','BarsController@new_participant');

Route::post('new/zone',         'ZonesController@new');
Route::get('edit/zone/{id}',    'ZonesController@edit');
Route::get('delete/zone/{id}',  'ZonesController@delete');
Route::post('save/zone',        'ZonesController@save');
Route::post('delete/zone',      'ZonesController@delete_zone');

Route::post('new/user',         'UsersController@new');
Route::get('edit/user/{id}',    'UsersController@edit');
Route::get('delete/user/{id}',  'UsersController@delete');
Route::post('save/user',        'UsersController@save');
Route::post('delete/user',      'UsersController@delete_user');
Route::post('export/users',     'UsersController@export');

Route::get('test','ContestantsController@test');

