<script>
    $(document).ready(function(){
        $('.modal').modal({dismissible:false});        
        $('#orders').DataTable();
        $('.sidenav').sidenav();
        $('.tooltipped').tooltip();
        $(".dropdown-trigger").dropdown({
            hover:true,
            coverTrigger:false
        });
        $('select').formSelect({hover:false});
        var prize_id;

         $('body').on('click', '.deletePrize', function(){
            prize_id = $(this).data('prize_id');
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('delete/prize')}}/"+prize_id,
                // Whether this is a POST or GET request
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function (response) {  
                if(response.status == 'error'){
                    var $toastContent = $('<span>'+ response.type +'</span>');
                    M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                }else if(response.status == 'success'){
                    $('.modal-content').html(response.data);
                    $('select').formSelect();
                    M.updateTextFields();
                    $('#modal2').modal('open');
                }
                },
                error: function(){
                //window.location.reload();
                var $toastContent = $('<span>Hubo un error en el servidor</span>');
                M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $('body').on('click', '.editPrize', function(){
            prize_id = $(this).data('prize_id');
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('edit/prize')}}/"+prize_id,
                // Whether this is a POST or GET request
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function (response) {   
                    if(response.status == 'error'){
                        var $toastContent = $('<span>'+ response.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                    }else if(response.status == 'success'){
                        $('.modal-content').html(response.data);
                        $('select').formSelect();
                        M.updateTextFields();
                        $('#modal1').modal('open');
                    }
                },
                error: function(){
                //window.location.reload();
                    var $toastContent = $('<span>Hubo un error en el servidor</span>');
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $('#btnCancelarEditar').click(function(){
            M.toast({html: 'Edicion Cancelada!', classes: 'rounded', displayLength:1000, completeCallback: function(){window.location.reload()}});                    
        });

        $('#btnCancelarEliminar').click(function(){
            M.toast({html: 'Eliminacion Cancelada!', classes: 'rounded', displayLength:1000, completeCallback: function(){window.location.reload()}});                    
        });

        $("#btnExportar").click(function(){            
            var jsonObject = {
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}"
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('export/prizes')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:1000, completeCallback: function(){window.location.reload()}});
                    }else{
                        M.toast({html: 'Exportacion Exitosa!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.open("{{url('/')}}/premios.xlsx", '_blank')}}); 
                        
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $('#btnEliminar').click(function(){
            var jsonObject = {
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}",
                prize_id : prize_id
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('delete/prize')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:1000, completeCallback: function(){window.location.reload()}});
                    }else{
                        M.toast({html: 'Premio Eliminado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        })

        $('#btnLogout2').click(function(){
            // Start $.ajax() method
            var jsonObject = {
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}"
            }
            $.ajax({
            // The URL for the request. variable set above
            url: "{{url('user/logout')}}",
            // The data to send (will be converted to a query string). variable set above
            data: jsonObject,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            // Whether this is a POST or GET request
            type: "POST",
            // The type of data we expect back. can be json, html, text, etc...
            dataType : "json",
            // Code to run if the request succeeds;
            // the response is passed to the function
            success: function( msg ) {
                if(msg.status == 'error'){
                var $toastContent = $('<span>'+ msg.type +'</span>');
                M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                }else{
                window.location.reload();
                }
            },
            error: function(){
                M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
            }
            });
        }); 

        $('#btnLogout').click(function(){
            // Start $.ajax() method
            var jsonObject = {
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}"
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('user/logout')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                    var $toastContent = $('<span>'+ msg.type +'</span>');
                    M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                    }else{
                        window.location.reload();
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });  

        $("#btnGuardarNuevo").click(function(){
             // Start $.ajax() method
             var jsonObject = {
                session_id      :   "{{$session_id}}",
                session_token   :   "{{$session_token}}",
                name            :   $("#new_name").val(),
                code            :   $("#new_code").val(),
                active          :   $("#new_active").val(),
                location        :   $("#new_location").val()
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('new/prize')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                    }else{
                        M.toast({html: 'Premio Agregado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $("#btnGuardar").click(function(){
            // Start $.ajax() method
            var jsonObject = {
                session_id      :   "{{$session_id}}",
                session_token   :   "{{$session_token}}",
                name            :   $("#name").val(),
                code            :   $("#code").val(),
                active          :   $("#active").val(),
                location        :   $("#location").val(),
                prize_id        :   prize_id
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('save/prize')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:1000, completeCallback: function(){window.location.reload()}});
                    }else{
                        M.toast({html: 'Premio Editado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        })
    });
</script>