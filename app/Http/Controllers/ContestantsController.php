<?php



namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

use App\Models\UserSessionsModel;

use App\Models\BarsModel;

use App\Models\LocationsModel;

use App\Models\ContestantsModel;

use App\Models\ZonesModel;

use App\Models\PrizesModel;

use Illuminate\Http\Request;

use Auth;



use PhpOffice\PhpSpreadsheet\Spreadsheet;

use PhpOffice\PhpSpreadsheet\Writer\Xlsx;



class ContestantsController extends BaseController

{

    public function __construct()

    {

        $this->middleware('auth');

    }



    public function export(Request $request)
    {
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
                    ->setCreator("Salvador Jimenez")
                    ->setLastModifiedBy("Salvador Jimenez")
                    ->setTitle("Participantes Recomienda HK")
                    ->setSubject("Lista de premios")
                    ->setDescription(
                        "Documento de Excel con los participantes de la plataforma recomiendahk.com"
                    )
                    ->setKeywords("RecomiendaHK")
                    ->setCategory("Participantes Exportados");
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1',  'Nombre');
        $sheet->setCellValue('B1',  'Establecimiento');
        $sheet->setCellValue('C1',  'DNI');
        $sheet->setCellValue('D1',  'Correo electrónico');
        $sheet->setCellValue('E1',  'Isla');
        $sheet->setCellValue('F1',  'Zona');
        $x=2;
        $contestants =   ContestantsModel::all();
        foreach($contestants as $contestant)
        {
            $bar    =   BarsModel::find($contestant->bar);
            $sheet->setCellValue('A'.$x,  $contestant->name);
            $sheet->setCellValue('B'.$x,  $bar->name);
            $sheet->setCellValue('C'.$x,  $contestant->dni);
            $sheet->setCellValue('D'.$x,  $contestant->email);
            $sheet->setCellValue('E'.$x,  LocationsModel::find($contestant->location)->name);
            $sheet->setCellValue('F'.$x,  ZonesModel::find($bar->zone)->name);
            $x++;
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save('participantes.xlsx');
        return response(array('status'  =>  'success'));
    }



    public function new(Request $request)

    {

        //Input Data

        $session_id     =   $request->input('session_id');

        $session_token  =   $request->input('session_token');

        $name           =   $request->input('name');

        $lastname       =   $request->input('lastname');

        $dni            =   $request->input('dni');

        $email          =   $request->input('email');

        $bar_id         =   $request->input('bar');

        $location_id    =   $request->input('location');

        $bases          =   $request->input('bases');

        $acepto_participar  =   $request->input('acepto_participar');

        $acepto_email       =   $request->input('acepto_email');

        $acepto_sorteo      =   $request->input('acepto_sorteo');



        if($bases != 1)

            return response(array('status'  =>  'error',    'type'  =>  'No haz aceptado los Terminos y Condiciones'));

        if($acepto_participar != 1)

            return response(array('status'  =>  'error',    'type'  =>  'No haz aceptado Participar en La Promocion'));

        /*if($acepto_email != 1)

            return response(array('status'  =>  'error',    'type'  =>  'No haz aceptado recibir Emails'));*/

        if($acepto_sorteo != 1)

            return response(array('status'  =>  'error',    'type'  =>  'No haz aceptado la Utilización de tus Datos en el sorteo'));

        if(empty($name))

            return response(array('status'  =>  'error',    'type'  =>  'Falta el Nombre'));

        if(empty($lastname))

            return response(array('status'  =>  'error',    'type'  =>  'Falta el Apellido'));

        if(empty($dni))

            return response(array('status'  =>  'error',    'type'  =>  'Falta el DNI'));

        if(empty($email))

            return response(array('status'  =>  'error',    'type'  =>  'Falta el Correo electrónico'));

        if(empty($bar_id))

            return response(array('status'  =>  'error',    'type'  =>  'Falta el Establecimiento'));

        if(empty($location_id))

            return response(array('status'  =>  'error',    'type'  =>  'Falta la Localidad'));



        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();

        if(!$user_session)

            return response(array('status'  =>  'error',    'type'  =>  'Acceso no autorizado'));

        

        $email_already_exists   =   ContestantsModel::where('email',$email)->first();

        if($email_already_exists)

            return response(array('status'  =>  'error',    'type'  =>  'Este correo electrónico es invalido'));

        $dni_already_exists     =   ContestantsModel::where('dni',$dni)->first();

        if($dni_already_exists)

            return response(array('status'  =>  'error',    'type'  =>  'Este DNI es invalido'));

        

        $bar_zone                   =   BarsModel::find($bar_id)->zone;

        $bar_location               =   ZonesModel::find($bar_zone)->location;

        if($bar_location != $location_id)

            return response(array('status'  =>  'error',    'type'  =>  'Este bar no esta dentro de esa localidad'));

        

        $new_contestant             =   new ContestantsModel();

        $new_contestant->name       =   ucwords($name);

        $new_contestant->last_name  =   ucwords($lastname);

        $new_contestant->dni        =   $dni;

        $new_contestant->email      =   strtolower($email);

        $new_contestant->bar        =   $bar_id;

        $new_contestant->location   =   $location_id;

        $new_contestant->save();



        return response(array('status'  =>  'success'));

    }



    public function edit($id=null)

    {

        $contestant     =   ContestantsModel::find($id);

        $locations      =   LocationsModel::all();

        $bars           =   BarsModel::all();

        $modal          =   '<h5>Editando al participante: <b>' . $contestant->name . ' ' . $contestant->last_name . '</b></h5>';

        $modal          .=  '<div class="row">

                            <div class="input-field col m6 l6 s12">

                            <i class="material-icons prefix">person</i>

                            <input id="name" name="name" type="text" class="validate" value="'.$contestant->name.'">

                            <label for="name">Nombre(s)</label>

                            </div>

                            <div class="input-field col m6 l6 s12">

                            <i class="material-icons prefix">person</i>

                            <input id="lastname" name="lastname" type="text" class="validate" value="'.$contestant->last_name.'">

                            <label for="lastname">Apellidos</label>

                            </div>

                        </div>

                        <div class="row">      

                            <div class="input-field col m6 l6 s12">

                            <i class="material-icons prefix">contact_mail</i>

                            <input id="dni" name="dni" type="text" class="validate" value="'.$contestant->dni.'">

                            <label for="dni">DNI</label>

                            </div>    

                            <div class="input-field col m6 l6 s12">

                            <i class="material-icons prefix">mail</i>

                            <input autocomplete="off" id="email" name="email" type="email" class="validate" value="'.$contestant->email.'">

                            <label for="email">Correo electrónico</label>

                            </div>        

                        </div>

                        <div class="row">

                            <div class="input-field col m6 l6 s12">

                            <i class="material-icons prefix">local_bar</i>

                            <select name="bar" id="bar">

                                <option value="">Establecimiento</option>';

                                foreach($bars as $bar)

                                {

                                    if($contestant->bar == $bar->id)

                                        $modal.='<option value="'.$bar->id.'" selected>'.$bar->name.'</option>';

                                    else

                                        $modal.='<option value="'.$bar->id.'">'.$bar->name.'</option>';

                                }

                            $modal.='</select>

                            <label for="bar">Establecimiento</label>

                            </div>

                            <div class="input-field col m6 l6 s12">

                            <i class="material-icons prefix">landscape</i>

                            <select name="location" id="location">

                                <option value="">Localidad</option>';

                                foreach($locations as $location)

                                {

                                    if($contestant->location == $location->id)

                                        $modal.='<option value="'.$location->id.'" selected>'.$location->name.'</option>';

                                    else

                                        $modal.='<option value="'.$location->id.'">'.$location->name.'</option>';

                                }

                            $modal.='</select>

                            <label for="location">Localidad</label>

                            </div>

                        </div>';

        //return response(array('status'  =>  'error',    'type'  =>  'prueba'));

        return response(array('status'  =>  'success',  'data'  =>  $modal));

    }



    public function delete($id=null)

    {

        $contestant     =   ContestantsModel::find($id);

        $modal          =   '<h5>Eliminando al participante: <b>' . $contestant->name . ' ' . $contestant->last_name . '</b></h5>';

        return response(array('status'  =>  'success',  'data'  =>  $modal));

    }



    public function delete_contestant(Request $request)

    {

        //Data Inputs

        $contestant_id  =   $request->input('contestant_id');



        if(empty($contestant_id))

            return response(array('status'  =>  'error',    'type'  =>  'Imposible eliminar a este participante'));

        

        $contestant_to_be_deleted   =   ContestantsModel::find($contestant_id);

        $are_there_prizes_linked_to_this_user =   PrizesModel::where('owner',$contestant_to_be_deleted->id)->first();

        if($are_there_prizes_linked_to_this_user)

            return response(array('status'  =>  'error',    'type'  =>  'Imposible eliminar este Usuario, existen Premios vinculados a el'));


        $contestant_to_be_deleted->delete();

        

        return response(array('status'  =>  'success'));

    }



    public function save(Request $request)

    {

        //Input Data

        $session_id     =   $request->input('session_id');

        $session_token  =   $request->input('session_token');

        $name           =   $request->input('name');

        $lastname       =   $request->input('lastname');

        $dni            =   $request->input('dni');

        $email          =   $request->input('email');

        $bar_id         =   $request->input('bar');

        $location_id    =   $request->input('location');

        $contestant_id  =   $request->input('contestant_id');



        $contestant     =   ContestantsModel::find($contestant_id);

        if(!$contestant)

            return response(array('status'  =>  'error',    'type'  =>  'Imposible Editar a este Concursante'));

        if(empty($name))

            return response(array('status'  =>  'error',    'type'  =>  'Falta el Nombre'));

        if(empty($lastname))

            return response(array('status'  =>  'error',    'type'  =>  'Falta el Apellido'));

        if(empty($dni))

            return response(array('status'  =>  'error',    'type'  =>  'Falta el DNI'));

        if(empty($email))

            return response(array('status'  =>  'error',    'type'  =>  'Falta el Correo electrónico'));

        if(empty($bar_id))

            return response(array('status'  =>  'error',    'type'  =>  'Falta el Establecimiento'));

        if(empty($location_id))

            return response(array('status'  =>  'error',    'type'  =>  'Falta la Localidad'));



        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();

        if(!$user_session)

            return response(array('status'  =>  'error',    'type'  =>  'Acceso no autorizado'));

        

        if(strcasecmp($email, $contestant->email) != 0)

        {

            $email_already_exists   =   ContestantsModel::where('email',$email)->first();

            if($email_already_exists)

                return response(array('status'  =>  'error',    'type'  =>  'Este correo electrónico es invalido'));

        }

        

        if(strcasecmp($dni,$contestant->dni) != 0)

        {

            $dni_already_exists     =   ContestantsModel::where('dni',$dni)->first();

            if($dni_already_exists)

                return response(array('status'  =>  'error',    'type'  =>  'Este DNI es invalido'));

        }

        

        $bar_zone                   =   BarsModel::find($bar_id)->zone;

        $bar_location               =   ZonesModel::find($bar_zone)->location;

        if($bar_location != $location_id)

            return response(array('status'  =>  'error',    'type'  =>  'Este bar no esta dentro de esa localidad'));

        

        $contestant->name       =   ucwords($name);

        $contestant->last_name  =   ucwords($lastname);

        $contestant->dni        =   $dni;

        $contestant->email      =   strtolower($email);

        $contestant->bar        =   $bar_id;

        $contestant->location   =   $location_id;

        $contestant->save();



        return response(array('status'  =>  'success'));

    }

}