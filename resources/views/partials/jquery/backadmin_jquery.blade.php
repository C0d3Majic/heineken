<script>
$(document).ready(function(){
  // Initialize collapse button
  $('#orders').DataTable();
  $('.sidenav').sidenav();
  $('select').formSelect();
  $(".dropdown-trigger").dropdown({
    hover:true,
    coverTrigger:false
  });
  $('.modal').modal();
  $('#btnLogout2').click(function(){
    // Start $.ajax() method
    var jsonObject = {
        session_id: "{{$session_id}}",
        session_token : "{{$session_token}}"
    }
    $.ajax({
      // The URL for the request. variable set above
      url: "{{url('user/logout')}}",
      // The data to send (will be converted to a query string). variable set above
      data: jsonObject,
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      // Whether this is a POST or GET request
      type: "POST",
      // The type of data we expect back. can be json, html, text, etc...
      dataType : "json",
      // Code to run if the request succeeds;
      // the response is passed to the function
      success: function( msg ) {
        if(msg.status == 'error'){
          var $toastContent = $('<span>'+ msg.type +'</span>');
          M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
        }else{
          window.location.reload();
        }
      },
      error: function(){
        M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
      }
    });
  }); 
  $('#btnLogout').click(function(){
    // Start $.ajax() method
    var jsonObject = {
        session_id: "{{$session_id}}",
        session_token : "{{$session_token}}"
    }
    $.ajax({
      // The URL for the request. variable set above
      url: "{{url('user/logout')}}",
      // The data to send (will be converted to a query string). variable set above
      data: jsonObject,
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      // Whether this is a POST or GET request
      type: "POST",
      // The type of data we expect back. can be json, html, text, etc...
      dataType : "json",
      // Code to run if the request succeeds;
      // the response is passed to the function
      success: function( msg ) {
        if(msg.status == 'error'){
          var $toastContent = $('<span>'+ msg.type +'</span>');
          M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
        }else{
          window.location.reload();
        }
      },
      error: function(){
        M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
      }
    });
  });  
});
</script>