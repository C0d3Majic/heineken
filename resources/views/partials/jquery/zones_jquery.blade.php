<script>
    $(document).ready(function(){
        $('.modal').modal({dismissible:false});        
        $('#orders').DataTable();
        $('.sidenav').sidenav();
        $('.tooltipped').tooltip();
        $(".dropdown-trigger").dropdown({
            hover:true,
            coverTrigger:false
        });
        $('select').formSelect({hover:false});
        var zone_id;

         $('body').on('click', '.deleteZone', function(){
            zone_id = $(this).data('zone_id');
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('delete/zone')}}/"+zone_id,
                // Whether this is a POST or GET request
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function (response) {  
                if(response.status == 'error'){
                    var $toastContent = $('<span>'+ response.type +'</span>');
                    M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                }else if(response.status == 'success'){
                    $('.modal-content').html(response.data);
                    $('select').formSelect();
                    M.updateTextFields();
                    $('#modal2').modal('open');
                }
                },
                error: function(){
                //window.location.reload();
                var $toastContent = $('<span>Hubo un error en el servidor</span>');
                M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $('body').on('click', '.editZone', function(){
            zone_id = $(this).data('zone_id');
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('edit/zone')}}/"+zone_id,
                // Whether this is a POST or GET request
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function (response) {   
                    if(response.status == 'error'){
                        var $toastContent = $('<span>'+ response.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                    }else if(response.status == 'success'){
                        $('.modal-content').html(response.data);
                        $('select').formSelect();
                        M.updateTextFields();
                        $('#modal1').modal('open');
                    }
                },
                error: function(){
                //window.location.reload();
                    var $toastContent = $('<span>Hubo un error en el servidor</span>');
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $('#btnCancelarEditar').click(function(){
            M.toast({html: 'Edicion Cancelada!', classes: 'rounded', displayLength:1000, completeCallback: function(){window.location.reload()}});                    
        });

        $('#btnCancelarEliminar').click(function(){
            M.toast({html: 'Eliminacion Cancelada!', classes: 'rounded', displayLength:1000, completeCallback: function(){window.location.reload()}});                    
        });

        $('#btnEliminar').click(function(){
            var jsonObject = {
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}",
                zone_id : zone_id
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('delete/zone')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:1000, completeCallback: function(){window.location.reload()}});
                    }else{
                        M.toast({html: 'Zona Eliminada Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        })
        
        $('#btnLogout2').click(function(){
            // Start $.ajax() method
            var jsonObject = {
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}"
            }
            $.ajax({
            // The URL for the request. variable set above
            url: "{{url('user/logout')}}",
            // The data to send (will be converted to a query string). variable set above
            data: jsonObject,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            // Whether this is a POST or GET request
            type: "POST",
            // The type of data we expect back. can be json, html, text, etc...
            dataType : "json",
            // Code to run if the request succeeds;
            // the response is passed to the function
            success: function( msg ) {
                if(msg.status == 'error'){
                var $toastContent = $('<span>'+ msg.type +'</span>');
                M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                }else{
                window.location.reload();
                }
            },
            error: function(){
                M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
            }
            });
        }); 

        $('#btnLogout').click(function(){
            // Start $.ajax() method
            var jsonObject = {
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}"
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('user/logout')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                    var $toastContent = $('<span>'+ msg.type +'</span>');
                    M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                    }else{
                        window.location.reload();
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });  

        $("#btnGuardarNuevo").click(function(){
             // Start $.ajax() method
             var jsonObject = {
                session_id      :   "{{$session_id}}",
                session_token   :   "{{$session_token}}",
                name            :   $("#new_name").val(),
                location        :   $("#new_location").val()
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('new/zone')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                    }else{
                        M.toast({html: 'Zona Agregada Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $("#btnGuardar").click(function(){
            // Start $.ajax() method
            var jsonObject = {
                session_id      :   "{{$session_id}}",
                session_token   :   "{{$session_token}}",
                name            :   $("#name").val(),
                location        :   $("#location").val(),
                zone_id         :   zone_id
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('save/zone')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:1000, completeCallback: function(){window.location.reload()}});
                    }else{
                        M.toast({html: 'Zona Editado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        })
    });
</script>