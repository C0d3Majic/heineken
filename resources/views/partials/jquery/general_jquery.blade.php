<script>
$(document).ready(function(){
    // Init Skrollr for 768 and up
    adjustWindow();    
    function adjustWindow(){
     
        // Get window size
        winH = $(window).height();
        winW = $(window).width();

        // Keep minimum height 550
        if(winH <= 550) {
            winH = 550;
        }
        // Init Skrollr for 768 and up
        if( winW >= 768) {
            $("#main").removeClass('MainContainer')
            $("#parallax-front").removeClass('ContentContainer')            
            $("#parallax-back").removeClass('ParallaxContainer')
        } else {            
            $("#main").addClass('MainContainer')
            $("#parallax-front").addClass('ContentContainer')
            $("#images").empty();
            $("#images").append('<img class="responsive-img" id="responsive" style="width: 20%;float:right;position: absolute;bottom: 15%;  " src="/img/iconos_hk_movil.png">')
            $("#parallax-back").addClass('ParallaxContainer')
        }       
    }

    function initAdjustWindow() {
        return {
            match : function() {
                adjustWindow();
            },
            unmatch : function() {
                adjustWindow();
            }
        };
    }
 
    enquire.register("screen and (min-width : 768px)", initAdjustWindow(), false);

  
    /* Parallax */
    /* 
    parallax -> Nunca debe ser 1 ya que entonces el fondo y el frente iran a la misma velocidad. 
    Si es mayor de 1 el frente irá más deprisa que el fondo. 
    Si es menor de 1 el fondo irá más deprisa que el frente. 
    */
    /*var parallax  = 3;
    $(document).scroll(function () {
        s = $(document).scrollTop();
        /* Efecto parallax *//*
        $(document).scroll(function () {
            s = $(document).scrollTop();
            $("#parallax-back").css("top", Math.round(s/parallax)  + "px");
            var newheight =  parseInt($("#parallax-front").css("height").replace("px","")) - Math.round(s/3);	
            $("#parallax-back").css("height", newheight + "px" );
        })
    })*/
    
    $("#buttonComprobar").click(function(){
            var jsonData = {
                dni : $("#inputDni").val()
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('valid/dni')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonData,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html:$toastContent, classes: 'rounded red', displayLength:2000});
                        
                    }else if(msg.status == 'success'){
                        M.toast({html: 'DNI aceptado!', classes: 'rounded', displayLength:2000, completeCallback: function(){window.location.reload()/*href = "{{url('codigo')}}"*/}});                    
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
            return false;
            //setTimeout(function(){ window.location.href = "{{url('codigo')}}" }, 2000);
    })
    $("#buttonGenerar").click(function(){
        var jsonData = {
                code : $("#inputDni").val()
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('valid/code')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonData,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html:$toastContent, classes: 'rounded red', displayLength:2000});
                        
                    }else if(msg.status == 'success'){
                        M.toast({html: 'Codigó aceptado!', classes: 'rounded', displayLength:2000, completeCallback: function(){window.location.href = "{{url('agradecimiento')}}"}});                    
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
            return false;     
    })
});
</script>