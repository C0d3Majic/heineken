<footer id="footer" class="page-footer" style="padding-top:5px">

    <div class="container">

      <div class="row" style="text-align:justify;margin-bottom:5px; font-size: 1vmin;">

     Promoción válida del 15 de mayo al 31 de julio de 2018. Participan en la promoción todos los puntos de venta 
     y camareros registrados por nuestro  “agente de reclutamiento”. Cada participante, registrado previamente, 
     podrá ser visitado por nuestros ” agentes secretos” y obtener premio cuando se cumpla con los requisitos de 
     las baes establecidas para la promoción “Recomienda Heineken”. Los premios consistirán en: (900) tarjetas 
     regalo por valor de 10€, (100) entradas conciertos o (12) suscripciones a Netflix, entregadas aleatoriamente 
     a través de la web <a href="{{url('/')}}"  style="color:white;display:inline !important;">www.recomiendahk.com</a>
     </br>Para los camareros participantes habrá un sorteo de 2 Iwatch (1 por provincia), 2 Ipad Mini (1 por Provinica) 
     y 1 Iphone X 64gb (para las dos provincias), que serán entregados aleatoriamente mediante la aplicación 
     Sorteados. El punto de venta participará en un sorteo de 2 viajes experiencias. Puede consultar las bases 
     completas de la promoción en <a href="{{url('bases')}}" style="color:white;display:inline !important;">www.recomiendahk.com</a>
     
      </div>
      <div class="row center">
      <a href="{{url('bases')}}"class="waves-effect waves-light btn-large" style="background-color:white;color:black"><i class="material-icons right">priority_high</i>Bases Legales</a>
      </div>
    </div>    

</footer>