@extends("partials.layouts.login_layout")

@section("content")
<!--
  Content Section Start
-->
<main>
    <div class="valign-wrapper" style="width:100%;height:100%;position: absolute;">
        <div class="valign" style="width:100%;">
            <center>
                <img class="responsive-img" style="width: 100px;" src="/img/logo.svg" />
                <h4 class="white-text">Acceso a backend</h4>
                <div class="container">
                    <div class="z-depth-1 white row" style="display: inline-block; padding: 32px 48px 0px 48px; background-color:#29C942">
                        <form class="col s12" class="login-form" id="login_form" method="post" action="{{url('user/login')}}">
                            {{ csrf_field() }}
                            <div class='row'>
                                <div class="input-field col s12">
                                    <input id="email" type="email" name="email" class="validate">
                                    <label for="email" class="active">Correo electrónico</label>
                                </div>
                            </div>
                            <div class='row'>
                                <div class="input-field col s12">
                                    <input id="password" type="password" name="password" class="validate">
                                    <label for="password" class="active">Password</label>
                                </div>
                                <label style='float: left;'>
                                    <a class='green-text' href='{{url("bases")}}'><b>Terminos y Condiciones</b></a>
                                </label>
                                <label style='float: right;'>
                                    <a class='green-text' href='{{url("recuperar")}}'><b>Olvidaste la contraseña?</b></a>
                                </label>
                            </div>
                            <center>
                            <div class='row'>
                                <a id="buttonLogin" name='btn_login' class='col s12 btn btn-large waves-effect green'>Iniciar Sesión</a>
                            </div>
                            </center>
                        </form>
                    </div>
                </div><!--a href="#!">Create account</a-->
            </center>
        </div>
    </div>
</main>
<!--
  Content Section End
-->
@endsection