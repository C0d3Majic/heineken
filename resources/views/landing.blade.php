@extends("partials.layouts.backadmin_bars")



@section("content")

<!--

  Content Section Start

-->

<main>

  <ul id="slide-out" class="sidenav sidenav-fixed nav" style="top:64px; background-color:#343a40">

    @if(Auth::check() && Auth::user()->user_type == 1)

      <li><a href="{{url('inicio')}}" style="color:#fff"><i class="material-icons" style="color:#fff">home</i>Inicio</a></li>

      <li><a href="{{url('premios')}}" style="color:#fff"><i class="material-icons" style="color:#fff">card_giftcard</i>Premios</a></li>

      <li><a href="{{url('participantes')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Participantes</a></li>

      <li><a href="{{url('establecimientos')}}" style="color:#fff"><i class="material-icons" style="color:#fff">local_bar</i>Establecimientos</a></li>        

      <!--li><a href="{{url('zonas')}}" style="color:#fff"><i class="material-icons" style="color:#fff">landscape</i>Zonas</a></li-->      

      <li><a href="{{url('usuarios')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Usuarios</a></li>        

      <li><a id="btnLogout2"style="color:#fff"><i class="material-icons" style="color:#fff">power_settings_new</i>Cerrar Sesión</a></li>        

    @else
    
    <li><a href="{{url('inicio')}}" style="color:#fff"><i class="material-icons" style="color:#fff">home</i>Inicio</a></li>

      <li><a href="{{url('participantes')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Participantes</a></li>

      <li><a href="{{url('establecimientos')}}" style="color:#fff"><i class="material-icons" style="color:#fff">local_bar</i>Establecimientos</a></li>        

      <li><a id="btnLogout2"style="color:#fff"><i class="material-icons" style="color:#fff">power_settings_new</i>Cerrar Sesión</a></li>        

    @endif

  </ul>

    <div class="valign-wrapper" style="width:100%;height:90%;position: absolute; background-color:#3d7d3c">

        <div class="valign" style="width:100%;">

            <center>

                <img class="responsive-img" style="width: 100px;" src="/img/logo.svg" />

                

            </center>

        </div>

    </div>

  </main>

<!--

  Content Section End

-->

@endsection