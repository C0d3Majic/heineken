@extends("partials.layouts.general_layout")

@section("content")
<!--
  Content Section Start
-->
<main id="main">
    <div id="parallax-back">
        <div style="text-align:center">
            <img id="texto_header" class="responsive-img" style="width: 93%;padding-top:20px;" src="/img/bg_texto_header_hk.png">
        </div>
        <div style="text-align:center">
            <img id="botellas" class="responsive-img" style="float:left;width: 53%;" src="/img/bg_botellas_hk.png">
        </div>
    </div>
    <div id="parallax-front">
            <img class="responsive-img" id="texto_subheader" style="width: 60%;float:right;position: absolute;right: 1%;padding-top: 10px;" src="/img/bg_texto_sub_header_hk.png">
            <img class="responsive-img" id="lupa" style="width: 65%;float:right;position: absolute;right: 0.1%;margin-top: 13%;" src="/img/bg_lupa_hk.png">
        <div id="inputs" style="width: 50%;float:right;position: absolute;right: 7%;margin-top: 30%;">
            <div class="row">
                <p id="text" class="center" style="font-weight:bold">Introduce tu DNI</br><span style="font-weight:bold; color:#3d7d3c !important">Tu regalo te espera</span></p>
                <input id="inputDni" type="text">
                <a id="buttonComprobar">COMPROBAR</a>
            </div>   
        </div>
        <div id="images">
            <img class="responsive-img" id="cards" style="width: 20%;float:right;position: absolute;bottom: 15%;  " src="/img/icono_tarjetas_regalo.png">
            <img class="responsive-img" id="tickets" style="width: 20%;float:right;position: absolute;bottom: 15%;right: 5%;" src="/img/icono_entradas_hk.png">
            <img class="responsive-img" id="netflix" style="width: 20%;float:right;position: absolute;bottom: 10%;  " src="/img/icono_netflix.png">
            <img class="responsive-img" id="travels" style="width: 20%;float:right;position: absolute;bottom: 10%;right: 5%;" src="/img/icono_viajes.png">
        </div>
        
    </div>
</main>
<!--
  Content Section End
-->
@endsection