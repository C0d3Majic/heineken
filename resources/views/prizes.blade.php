@extends("partials.layouts.backadmin_prizes")



@section("content")

<!--

  Content Section Start

-->

<main>

  <container>

    <ul id="slide-out" class="sidenav sidenav-fixed nav" style="top:64px; background-color:#343a40">

      @if(Auth::check() && Auth::user()->user_type == 1)

        <li><a href="{{url('inicio')}}" style="color:#fff"><i class="material-icons" style="color:#fff">home</i>Inicio</a></li>

        <li><a href="{{url('premios')}}" style="color:#fff"><i class="material-icons" style="color:#fff">card_giftcard</i>Premios</a></li>

        <li><a href="{{url('participantes')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Participantes</a></li>

        <li><a href="{{url('establecimientos')}}" style="color:#fff"><i class="material-icons" style="color:#fff">local_bar</i>Establecimientos</a></li>        

        <!--li><a href="{{url('zonas')}}" style="color:#fff"><i class="material-icons" style="color:#fff">landscape</i>Zonas</a></li-->      

        <li><a href="{{url('usuarios')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Usuarios</a></li>        

        <li><a id="btnLogout2"style="color:#fff"><i class="material-icons" style="color:#fff">power_settings_new</i>Cerrar Sesión</a></li>        

      @else

        <li><a href="{{url('participantes')}}" style="color:#fff"><i class="material-icons" style="color:#fff">people</i>Participantes</a></li>

        <li><a href="{{url('establecimientos')}}" style="color:#fff"><i class="material-icons" style="color:#fff">local_bar</i>Establecimientos</a></li>        

        <li><a id="btnLogout2"style="color:#fff"><i class="material-icons" style="color:#fff">power_settings_new</i>Cerrar Sesión</a></li>        

      @endif

    </ul>

    <div class="row">

      <div class="col xl9 offset-xl3 l8 offset-l4 m12 s12">

          <h3 class="title">Premios</h3>        

      </div>  

      <div class="col xl9 offset-xl3 l8 offset-l4 m12 s12">

        <div class="card blue-white darken-1">

          <div class="card-content black-text">

            <div class="row">

              <div class="col s12" style="overflow-x:auto;">

                <table id="orders" class="striped">

                <thead>

                    <tr>

                        <th>Código</th>

                        <th>Premio</th>

                        <th>Activo</th>

                        <th>Ubicación</th>

                        <th>Acciones</th>

                    </tr>

                </thead>

                <tbody>

                  @foreach($prizes as $prize)

                    <tr>

                      <td>{{$prize->code}}</td>

                      <td>{{$prize->name}}</td>

                      <td>@if($prize->is_active) Si @else No @endif</td>

                      <td>{{$prize->location}}</td>

                      <td>

                          <a class="editPrize small material-icons tooltipped" data-prize_id="{{$prize->id}}" data-position="bottom" data-delay="50" data-tooltip="Editar"><i class="small material-icons">mode_edit</i></a>

                          <a class="deletePrize small material-icons tooltipped" data-prize_id="{{$prize->id}}" data-position="bottom" data-delay="50" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a>

                      </td>  

                    </tr>

                  @endforeach

                </tbody>

                </table>

              </div>

            </div>

          </div>

        </div>    

      </div>            

    </div>   

    

    <div class="row">

      <div class="col l2 push-l6 m2 push-m4 s6">

        <a id="btnAgregar" class="waves-effect waves-light btn modal-trigger" href="#modal3">Agregar nuevo premio</a>

      </div>

      <div class="col l4 push-l6 m4 push-m4 s6">

        <a id="btnExportar" class="waves-effect waves-light btn">Exportar lista de premios</a>

      </div>

    </div>

  </container>

</main>

<!-- Modal Structure -->

<div id="modal1" class="modal">

  <div class="modal-content">

  </div>

  <div class="modal-footer">

    <div class="row">

      <div class="col s6">

        <a id="btnGuardar" class="modal-action modal-close waves-effect waves-light btn">Save changes</a>

      </div>

      <div class="col s6">

        <a id="btnCancelarEditar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancel changes</a>

      </div>

    </div>

  </div>

</div>



<!-- Modal Structure -->

<div id="modal2" class="modal">

  <div class="modal-content">

    <h4>Delete Order</h4>

    <p>Do you really want to delete this order?</p>

  </div>

  <div class="modal-footer">

    <div class="row">

      <div class="col s6">

        <a id="btnEliminar" class="modal-action modal-close waves-effect waves-light btn">Confirm</a>

      </div>

      <div class="col s6">

        <a id="btnCancelarEliminar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancel</a>

      </div>

    </div>

  </div>

</div>



<!-- Modal Structure -->

<div id="modal3" class="modal">

  <div class="modal-content">

    <h4 class="center">Llena los datos para el nuevo premio </h4>

    <div class="row">

      <div class="input-field col m6 l6 s12">

        <i class="material-icons prefix">vpn_key</i>

        <input id="new_code" name="new_code" type="text" class="validate">

        <label for="new_code">Código</label>

      </div>  

      <div class="input-field col m6 l6 s12">

        <i class="material-icons prefix">card_giftcard</i>

        <input id="new_name" name="new_name" type="text" class="validate">

        <label for="new_name">Premio</label>

      </div>      

    </div>

    <div class="row">

      <div class="input-field col m6 l6 s12">

        <i class="material-icons prefix">power_settings_new</i>

        <select name="new_active" id="new_active">

          <option value="" disabled selected>Esta activo?</option>

          <option value="1">Si</option>

          <option value="0">No</option>

        </select>

        <label for="new_active">Activo</label>

      </div>      

      <div class="input-field col m6 l6 s12">

        <i class="material-icons prefix">landscape</i>

        <select name="new_location" id="new_location">

          <option value="" disabled selected>En que localidad esta activo el premio?</option>

          @foreach($locations as $location)

            <option value="{{$location->id}}">{{$location->name}}</option>

          @endforeach

        </select>

        <label for="new_location">Localidad</label>

      </div>    

    </div>    

  </div>  

  <div class="modal-footer">

    <div class="row">

      <div class="col s4">

        <a id="btnGuardarNuevo" class="modal-action modal-close waves-effect waves-light btn">Guardar</a>

      </div>

      <div class="col s4 push-s2">

        <a id="btnCancelar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>

      </div>

    </div>

  </div>

</div>

<!--

  Content Section End

-->

@endsection